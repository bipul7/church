// delete banner
 $(document).on('click', '.btn_delete_banner', function(){         
     var id=$(this).data("delete_banner");
     var x=$(this);
      if(confirm("Are you sure you want to delete this?"))  
      {  
        $.ajax({  
         url:"admin/delete-banner",  
         method:"post",  
         data:{banner_key:id},
         dataType:"text",  
         success:function(data){  
                  x.closest('tr').fadeOut(); 
                }  
        });  
      }  
    }); 


 // edit banner form
  $(document).on('click','.edit_banner_item', function(){
        var banner_id   = $(this).attr('banner-key'); 

       $.ajax({
            url:'admin/render-edit-banner',
            method:'POST',
            data:{id:banner_id},
            success:function(data){
                var hhh = JSON.parse(data);
                $('#edit_banner_div').html(hhh.edit_banner_div);
            }
        })
    });


  // dropzone for banner image upload
   Dropzone.autoDiscover = false;

  $( document ).ready(function() { 
    var photo_upload = new Dropzone(".dropzone", {
        url: "AdminBannerController/saveImage",
        maxFilesize: 20,
        maxFiles: 1,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png",
        paramName: "userfile",
        dictInvalidFileType: "Type file ini tidak dizinkan",
        addRemoveLinks: true,
        init: function () {
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('.previews').
                        append(
                            "<input type='hidden' name='image' value='" + obj + "'>\n\
                           <input type='hidden' name='width' value='" + file.width + "'>\n\
                           <input type='hidden' name='height' value='" + file.height + "'>"
                                );

            });
        } 
    });
  });


  // multiple banner button
  //Clone the hidden element and shows it
  $('.add-one').click(function(){
    $('.dynamic-element').first().clone().appendTo('.dynamic-anchor').show();
    attach_delete();
  });
  //Attach functionality to delete buttons
  function attach_delete(){
    $('.delete').off();
    $('.delete').click(function(){
      console.log("click");
      $(this).closest('.form-groupp').remove();
    });
  }


  // add banner form validation
   $(document).ready(function(){      
      $("#addSlideshow").validate({
        rules: {    
          banner_image_title: {
            required: true
          }, 
          banner_image_subtitle: {
            required: true
          },
          banner_order: {
            required: true
          },         
        }
    });
  })
   
   $('#addSlideshow').on('submit', function(){
     $("#addSlideshow").valid();
   });




   // dataTable    
  document.addEventListener('DOMContentLoaded', function () { 
    var table;
      table = $('#banner').DataTable({

          processing: true, //Feature control the processing indicator.
          serverSide: true, //Feature control DataTables' server-side processing mode.
          //order: [], //Initial no order.
          // Load data for the table's content from an Ajax source
          ajax: {
              url: "admin/load-banner-table",
              type: "post",
              complete: function (res) {}
          },
          //Set column definition initialisation properties.
          columnDefs: [
              {
                  "targets": [-1], //last column
                  "orderable": false, //set not orderable
              },
          ],
          "ordering": false

      });
  });