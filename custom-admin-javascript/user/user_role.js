// add form validation
  $(document).ready(function(){      
      $("#addUserRole").validate({        
    });
  })
   $('#addUserRole').on('submit', function(){
     $("#addUserRole").valid();
   });





// edit role
$(document).on('click','.edit_role',function(){
       var role_key   = $(this).attr('role-key');
       $.ajax({
            url:'admin/render-edit-user-role',
            method:'POST',
            data:{id:role_key},

            success:function(data){
                var page_data = JSON.parse(data);
                $('#edit_user_role_div').html(page_data.edit_user_role_div);
            }
        })
    });


// dataTable    
  document.addEventListener('DOMContentLoaded', function () {
    var table;
      table = $('#roleTable').DataTable({
          processing: true, //Feature control the processing indicator.
          serverSide: true, //Feature control DataTables' server-side processing mode.
          //order: [], //Initial no order.
          // Load data for the table's content from an Ajax source
          ajax: {
              url: "admin/load-role-table",
              type: "post",
              complete: function (res) {}
          },
          //Set column definition initialisation properties.
          columnDefs: [
              {
                  "targets": [-1], //last column
                  "orderable": false, //set not orderable
              },
          ],

      });
  });