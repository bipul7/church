// staff type delete 
 $(document).on('click', '.btn_delete_type', function(){         
	 var id=$(this).data("id1");
	  if(confirm("Are you sure you want to delete this?"))  
	  {  
		$.ajax({  
		 url:"admin/delete-staff-type",  
		 method:"post",  
		 data:{staff_key:id},
		 dataType:"text",  
		 success:function(data){  
				  $("#"+id).fadeOut(); 
				}  
		});  
	  }  
	}); 


// edit type
$('.modalOpen').on('click', function(){
        var type_id   = $(this).closest('tr').attr('id'); 
        var staff_type_name= $(this).parent('tr').find('#staff_type_name').text();
        $("#e_staff_type_name").val(staff_type_name);
        $("#e_staff_type_key").val(type_id);
      
    });


// add form validation
$(document).ready(function(){      
      $("#addStaffType").validate({
        rules: {    
          staff_type_name: {
            required: true
          },
          
        }
    });
  })

  $('#addStaffType').on('submit', function(){
     $("#addStaffType").valid();
  });


  // edit form validation
    $(document).ready(function(){      
      $("#editStaffType").validate({
        rules: {    
          staff_type_name: {
            required: true
          },
          
        }
    });
  })
  $('#editStaffType').on('submit', function(){
     $("#editStaffType").valid();
  });


  //mass select
   jQuery('#master').on('click', function(e) {
	  if($(this).is(':checked',true))  
	  {
	    $(".sub_chk").prop('checked', true);  
	  }  
	  else  
	  {  
	    $(".sub_chk").prop('checked',false);  
	  }  
	});


   //mass delete
    $(document).on('click', '.delete_all', function(){         
         var allVals = [];  
          $(".sub_chk:checked").each(function() {  
              allVals.push($(this).attr('data-id'));
            });  
            //alert(allVals.length); return false;  
            if(allVals.length ==0) 
            {  
              alert("Please select atleast one row.");  
            }  
             else {  
      //$("#loading").show(); 
      WRN_PROFILE_DELETE = "Are you sure you want to delete all the selected row?";  
      var check = confirm(WRN_PROFILE_DELETE);  
      if(check == true){  
        //for server side
        
        var join_selected_values = allVals.join(","); 
        
        $.ajax({ 
          type: "POST",  
          url: "admin/mass-staff-type-delete",  
          cache:false,  
          data: 'ids='+join_selected_values,  
          success: function(response)  
          {   
            //$("#loading").hide();  
            //$("#msgdiv").html(response);
            //referesh table
            location.reload();
          }   
        });
              //for client side
        // $.each(allVals, function( index, value ) {
        //   $('table tr').filter("[data-row-id='" + value + "']").remove();
        // });
        

      }  
    } 
            
        }); 
