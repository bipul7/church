// delete staff

$(document).on('click', '.btn_delete_staff', function(){         
         var id=$(this).data("delete_staff");
         var x=$(this); 
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"admin/delete-staff",  
             method:"post",  
             data:{staff_key:id},
             dataType:"text",  
             success:function(data){
                       x.closest('tr').fadeOut(); 
                    }  
            });  
          }  
        }); 


// edit staff
$(document).on('click','.edit_staff', function(){
        var staff_id   = $(this).attr('staff-key'); 
       $.ajax({
            url:'admin/render-edit-staff',
            method:'POST',
            data:{id:staff_id},

            success:function(data){
                var hhh = JSON.parse(data);
                $('#edit_staff_div').html(hhh.edit_staff_div);
            }
        })
    });


// add staff image upload by dropzone
 Dropzone.autoDiscover = false;
  $( document ).ready(function() { 
    var photo_upload = new Dropzone(".dropzone", {
        url: "AdminStaffController/saveImage",
        maxFilesize: 50,
        maxFiles: 50,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png",
        paramName: "userfile",
        dictInvalidFileType: "Type file ini tidak dizinkan",
        addRemoveLinks: true,
        init: function () {
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;

                 $('.previews').
                        append(
                            "<input type='hidden' name='image[]' value='" + obj + "'>\n\
                           "
                                );

            });
        } 
    });
  });


  // multiple socialLink add 
  //Clone the hidden element and shows it
  $('.add-one').click(function(){
    $('.dynamic-element').first().clone().appendTo('.dynamic-anchor').show();
    attach_delete();
  });


  //Attach functionality to delete buttons
  function attach_delete(){
    $('.delete').off();
    $('.delete').click(function(){
      console.log("click");
      $(this).closest('.form-groupp').remove();
    });
  }


  // add form validation
  $(document).ready(function(){      
      $("#addStaff").validate({
        rules: {    
          staff_name: {
            required: true
          }, 
          staff_type_id: {
            required: true
          },
          staff_order: {
            required: true
          },
          email: {
            required: true
          }, 
          password: {
            required: true
          },        
        }
    });
  })
   $('#addStaff').on('submit', function(){
     $("#addStaff").valid();
   });



   // dataTable    
  document.addEventListener('DOMContentLoaded', function () { 
    var table;
      table = $('#staff_table').DataTable({

          processing: true, //Feature control the processing indicator.
          serverSide: true, //Feature control DataTables' server-side processing mode.
          //order: [], //Initial no order.
          // Load data for the table's content from an Ajax source
          ajax: {
              url: "admin/load-staff-table",
              type: "post",
              complete: function (res) {}
          },
          //Set column definition initialisation properties.
          columnDefs: [
              {
                  "targets": [-1], //last column
                  "orderable": false, //set not orderable
              },
          ],
          "ordering": false

      });
  });