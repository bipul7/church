
// instant view image when select image for upload
   function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#viewHeaderImage_e')
                        .attr('src', e.target.result)
                        .width(200)
                        
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

// edit form validation
  $(document).ready(function(){      
      $("#editPage").validate();
  })
   $('#editPage').on('submit', function(){
     $("#editPage").valid();
   });

   //add ck editor 
  $(document).ready(function () {
        CKEDITOR.replace('page_content_e');
});



 