$(document).ready(function () {

    //give the pairs of input and preview ids here
    var input_preview_pairs = {
        "site_logo": "site_logo_preview",
        "site_icon": "site_icon_preview"
    };

    init_img_preview(input_preview_pairs);

    function init_img_preview(input_preview_pairs) {

        console.log(input_preview_pairs);

        for (var key in input_preview_pairs) {
            if (input_preview_pairs.hasOwnProperty(key)) {
                change_image_preview(key, input_preview_pairs[key]);
            }

        }
    }

    function change_image_preview(input_id,preview_id) {
        console.log($("#"+input_id));
        $("#"+input_id).change(function () {
            readURL(this,"#"+preview_id);
        });
    }

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input,preview_selector_id) {

        console.log('f<s>');
        console.log(input.files[0]);
        console.log('f<e>');
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                console.log(input);
                console.log(preview_selector_id);
                $(preview_selector_id).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    CKEDITOR.replace('footer_text');
    //CKEDITOR.replace('address');
});

