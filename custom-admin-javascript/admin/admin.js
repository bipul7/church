// add form validation
  $(document).ready(function(){      
      $("#addAdmin").validate({
        rules: {    
          username: {
            required: true
          }, 
          email: {
            required: true,
            email: true,
          },
          password: {
            required: true
          },         
        }
    });
  })
   $('#addAdmin').on('submit', function(){
     $("#addAdmin").valid();
   });



  // admin image upload by dropzone
  Dropzone.autoDiscover = false;
  $( document ).ready(function() { 
    var photo_upload = new Dropzone(".dropzone", {
        url: "AdminController/saveImage",
        maxFilesize: 20,
        maxFiles: 1,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png",
        paramName: "userfile",
        dictInvalidFileType: "Type file ini tidak dizinkan",
        addRemoveLinks: true,
        init: function () {
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('.previews').
                        append(
                            "<input type='hidden' name='image' value='" + obj + "'>\n\
                           <input type='hidden' name='width' value='" + file.width + "'>\n\
                           <input type='hidden' name='height' value='" + file.height + "'>"
                                );

            });
        } 
    });
  });


  // delete gallery
$(document).on('click', '.btn_delete_admin', function(){         
         var id=$(this).data("id1");
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"admin/delete-admin",  
             method:"post",  
             data:{admin_key:id},
             dataType:"text",  
             success:function(data){  
                      $("#"+id).fadeOut(); 
                    }  
            });  
          }  
        });

// edit admin
// edit staff
$('.edit_admin').on('click', function(){
        var admin_id   = $(this).closest('tr').attr('id');
       $.ajax({
            url:'admin/render-edit-admin',
            method:'POST',
            data:{id:admin_id},

            success:function(data){
                var page_data = JSON.parse(data);
                $('#edit_admin_div').html(page_data.edit_admin_div);
            }
        })
    });
