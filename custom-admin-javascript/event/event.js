//google map implementation
function initMap() {
       map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 30.4326283, lng: -90.244281},
            zoom: 9
        });



        var input = document.getElementById('searchInput');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29),
            draggable:true,
        });


          map.addListener('click',function(event) {
            marker.setPosition(event.latLng);
            document.getElementById('latitude').value = event.latLng.lat();
            document.getElementById('longitude').value = event.latLng.lng();
            infowindow.setContent('Latitude: ' + event.latLng.lat() + '<br>Longitude: ' + event.latLng.lng());
            infowindow.open(map,marker);        
        });
    
        google.maps.event.addListener(marker,'dragend',function(event) {
            document.getElementById('latitude').value = event.latLng.lat();
            document.getElementById('longitude').value = event.latLng.lng();
            infowindow.setContent('Latitude: ' + event.latLng.lat() + '<br>Longitude: ' + event.latLng.lng());
            infowindow.open(map,marker);
        });




        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();

            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }

        // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
        /*
        marker.setIcon(({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        */
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
            infowindow.open(map, marker);

        
            document.getElementById('latitude').value = place.geometry.location.lat();
            document.getElementById('longitude').value = place.geometry.location.lng();
            document.getElementById('location').value = place.formatted_address;
        });


}





 //DateTime picker
$(document).ready(function(){
   $('.datepicker').datetimepicker({});
});


// event image upload by dropzone
 Dropzone.autoDiscover = false;
  $( document ).ready(function() { 
    var photo_upload = new Dropzone(".dropzone", {
        url: "AdminEventController/saveImage",
        maxFilesize: 50,
        maxFiles: 50,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png",
        paramName: "userfile",
        dictInvalidFileType: "Type file ini tidak dizinkan",
        addRemoveLinks: true,
        init: function () {
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;

                 $('.previews').
                        append(
                            "<input type='hidden' name='image[]' value='" + obj + "'>\n\
                           "
                                );

            });
        } 
    });
  });




  // edit event
$(document).on('click','.edit_event', function(){
       var event_id   = $(this).attr('event-key');         
       $.ajax({
            url:'admin/render-edit-event',
            method:'POST',
            data:{id:event_id},

            success:function(data){
                var hhh = JSON.parse(data);
                $('#edit_event_div').html(hhh.edit_event_div);
            }
        })
    });



// add form validation
  $(document).ready(function(){      
      $("#addEvent").validate({ });
  })
   $('#addEvent').on('submit', function(){
     $("#addEvent").valid();
   });


// delete event
$(document).on('click', '.btn_delete_event', function(){         
         var id=$(this).data("delete_event");
         var x=$(this); 
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"admin/delete-event",  
             method:"post",  
             data:{event_key:id},
             dataType:"text",  
             success:function(data){
                       x.closest('tr').fadeOut(); 
                    }  
            });  
          }  
        });


//mass select
   jQuery('#master').on('click', function(e) {
    if($(this).is(':checked',true))  
    {
      $(".sub_chk").prop('checked', true);  
    }  
    else  
    {  
      $(".sub_chk").prop('checked',false);  
    }  
  });


   //mass delete
    $(document).on('click', '.delete_all', function(){         
         var allVals = [];  
          $(".sub_chk:checked").each(function() {  
              allVals.push($(this).attr('data-id'));
            });   
            if(allVals.length ==0) 
            {  
              alert("Please select atleast one row.");  
            }  
             else {  
      WRN_PROFILE_DELETE = "Are you sure you want to delete all the selected row?";  
      var check = confirm(WRN_PROFILE_DELETE);  
      if(check == true){  
        //for server side        
        var join_selected_values = allVals.join(",");        
        $.ajax({ 
          type: "POST",  
          url: "admin/mass-event-delete",  
          cache:false,  
          data: 'ids='+join_selected_values,  
          success: function(response)  
          {  
            location.reload();
          }   
        });       

      }  
    } 
            
        }); 


       // dataTable    
  document.addEventListener('DOMContentLoaded', function () { 
    var table;
      table = $('#event_table').DataTable({

          processing: true, //Feature control the processing indicator.
          serverSide: true, //Feature control DataTables' server-side processing mode.
          //order: [], //Initial no order.
          // Load data for the table's content from an Ajax source
          ajax: {
              url: "admin/load-event-table",
              type: "post",
              complete: function (res) {}
          },
          //Set column definition initialisation properties.
          columnDefs: [
              {
                  "targets": [-1], //last column
                  "orderable": false, //set not orderable
              },
          ],
          "ordering": false

      });
  });