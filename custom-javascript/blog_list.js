document.addEventListener("DOMContentLoaded", function () {
    var page = 1;

    var $blog_list = $(".blog_list");
    var base_url = $blog_list.attr('base-url');
    var $footer = $("footer");
    var more_data = true;
    var $ajax_load = $('.ajax-load');

    //initially
    loadMoreData(page);

    $(window).scroll(function () {
        var load_more_condition = $(window).scrollTop() + $(window).height() >= $(document).height() - $footer.height() && more_data;
        //load_more_condition = true;
        if (load_more_condition) {
            page++;
            loadMoreData(page);
        }

    });


    function loadMoreData(page) {
        var url = base_url + 'ajax-blog-list-items/' + '?page=' + page;
        $.ajax(
            {
                url: url,
                type: "get",
                beforeSend: function () {
                    $ajax_load.show();
                }
            })

            .done(function (data) {
                console.log(data);
                if (data == " ") {
                    $ajax_load.html("No more records found");
                    $ajax_load.show();
                    more_data = false;
                    return;
                }

                $ajax_load.hide();
                $(".blog_list").append(data);
            })

            .fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('server not responding...');
            });
    }

})