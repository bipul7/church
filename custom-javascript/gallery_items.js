document.addEventListener("DOMContentLoaded", function () {

    //wow js for image animation
   /* wow = new WOW(
        {
            boxClass: 'wow',      // default
            animateClass: 'animated', // default
            offset: 0,          // default
            mobile: true,       // default
            live: true        // default
        }
    )
    wow.init();*/
    //----------------------------------------
    var page = 1;


    var $gallery_items = $(".gallery_items");
    var base_url = $gallery_items.attr('base-url');
    var $footer = $("footer");
    var more_data = true;
    var $ajax_load = $('.ajax-load');
    var category_key = "";

    //initially
    loadMoreData(page, category_key);

    $(window).scroll(function () {

        var load_more_condition = $(window).scrollTop() + $(window).height() >= $(document).height() - $footer.height() && more_data;

        //load_more_condition = true;

        if (load_more_condition) {

            page++;

            loadMoreData(page, category_key);

        }

    });


    function loadMoreData(page, category_key) {

        var url = base_url + 'ajax-gallery-items/' + '?page=' + page + "&category_key=" + category_key;

        console.log(base_url);
        console.log(url);
        $.ajax(
            {

                url: url,

                type: "get",

                beforeSend: function () {

                    $ajax_load.show();

                }

            })

            .done(function (data) {

                console.log(data);


                if (data == " ") {

                    $ajax_load.html("No more records found");
                    $ajax_load.show();
                    more_data = false;
                    return;

                }

                $ajax_load.hide();
                init_wow();
                $gallery_items.append(data);

            })

            .fail(function (jqXHR, ajaxOptions, thrownError) {

                alert('server not responding...');

            });

    }

    function init_wow(){
        wow = new WOW(
            {
                boxClass: 'wow',      // default
                animateClass: 'animated', // default
                offset: 0,          // default
                mobile: true,       // default
                live: true        // default
            }
        )
        wow.init();
    }

    $(".gallery-cat-btn").on('click', function () {
        $(".gallery-cat-btn").removeClass('btndark');
        $(".gallery-cat-btn").addClass('btndarkborder');
        $(this).removeClass('btndarkborder').addClass('btndark');
        $gallery_items.empty();
        category_key = $(this).attr('gallery-category-key');
        page = 1;
        loadMoreData(page, category_key);
    });

});