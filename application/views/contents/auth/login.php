 <section id="page_title" class="gradintblue">  
      <div class="banner_img"><img src="<?= $this->config->item('public_asset_path') ?>img/banner.jpg" class="img-fluid"></div>
      <div class="banner_content">
        <div class="container text-center">
          <h1>Login</h1>
          <ul class="bradcumb">
            <li><a href="index.html">Home</a></li>
            <li> / </li>
            <li>Login</li>
          </ul>
          
        </div>
      </div>  
    </section>
    
    <section id="main_contant">
      <div class="container"> 
          <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
               <!-- success and error msg showing area -->
                <div class="row">                
                        <div class="col-md-12"> 
                        <?php if($this->session->userdata('error_msg')){ ?>
                        <div class="alert alert-danger">                    
                            <span><?=$this->session->userdata('error_msg');?></span>
                  <a style="float: right;" href="#" class="close" data-dismiss="alert">&times;</a>
                        </div>
                         <?php } $this->session->unset_userdata('error_msg');?>
                        <?php if($this->session->userdata('success_msg')){ ?>
                        <div class="alert alert-success">                    
                            <span><?=$this->session->userdata('success_msg');?></span>
                  <a style="float: right;" href="#" class="close" data-dismiss="alert">&times;</a>
                        </div>
                         <?php } $this->session->unset_userdata('success_msg'); ?>
                        </div>                
                </div>
              <!-- success and error msg showing area end-->

                  <form method="post" action="check-login" id="loginForm">

                    <div class="card">
                      <div class="card-header">
                        <h4>Login</h4>
                      </div>
                      <div class="card-body">
                        <p class="card-text">
                          <div class="col">
                              <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" name="email" class="form-control" id="email" placeholder="Enter Email" required="required">
                              </div>
                            </div>

                            <div class="col">
                              <div class="form-group">
                                  <label for="password">Password</label>
                                  <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password" required="required">
                                </div>
                            </div> 
                            <div class="col">
                                <button type="submit" class="btn btndark">Submit</button>
                              </div> 
                        </p>                        
                      </div>
                    </div>                    
                  </form> 
            </div>
            <div class="col-md-3"></div>
          </div>
       </div>
    </section>