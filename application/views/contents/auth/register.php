 <section id="page_title" class="gradintblue">  
      <div class="banner_img"><img src="<?= $this->config->item('public_asset_path') ?>img/banner.jpg" class="img-fluid"></div>
      <div class="banner_content">
        <div class="container text-center">
          <h1>Register</h1>
          <ul class="bradcumb">
            <li><a href="/">Home</a></li>
            <li> / </li>
            <li>Register</li>
          </ul>
          
        </div>
      </div>  
    </section>
    
     <section id="main_contant">
      <div class="container"> 
          <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
               <!-- success and error msg showing area -->
                <div class="row">                
                        <div class="col-md-12"> 
                        <?php if($this->session->userdata('error_msg')){ ?>
                        <div class="alert alert-danger">                    
                            <span><?=$this->session->userdata('error_msg');?></span>
                  <a style="float: right;" href="#" class="close" data-dismiss="alert">&times;</a>
                        </div>
                         <?php } $this->session->unset_userdata('error_msg');?>
                        <?php if($this->session->userdata('success_msg')){ ?>
                        <div class="alert alert-success">                    
                            <span><?=$this->session->userdata('success_msg');?></span>
                  <a style="float: right;" href="#" class="close" data-dismiss="alert">&times;</a>
                        </div>
                         <?php } $this->session->unset_userdata('success_msg'); ?>
                        </div>                
                </div>
              <!-- success and error msg showing area end-->

                  <form method="post" action="register" id="loginForm">
                    <div class="card">
                      <div class="card-header">
                        <h4>Register</h4>
                      </div>
                      <div class="card-body">
                        <p class="card-text">
                           
                           <div class="col">
                              <div class="form-group">
                                <label for="email">Name</label>
                                <input type="text" name="user_username" class="form-control" id="user_name" placeholder="Enter Name" required="required">
                              </div>
                            </div>

                          <div class="col">
                              <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" name="user_email" class="form-control" id="email" placeholder="Enter Email" required="required">
                              </div>
                            </div>

                            <div class="col">
                              <div class="form-group">
                                  <label for="password">Password</label>
                                  <input type="password" name="user_password" class="form-control" id="password" placeholder="Enter Password" required="required">
                                </div>
                            </div>
                            
                            <div class="col">
                              <div class="form-group">
                                <label for="email">Phone</label>
                                <input type="number" name="user_phone" class="form-control" id="user_phone" placeholder="Enter Phone" required="required">
                              </div>
                            </div>

                            <div class="col">
                              <div class="form-group">
                                <label for="email">Gender</label>
                                <select name="user_gender" class="form-control" id="user_gender" required="required">
                                  <option value="">-</option>
                                  <option value="male">Male</option>
                                  <option value="male">Female</option>
                                </select>
                              </div>
                            </div>

                            <div class="col">
                                <button type="submit" class="btn btndark">Submit</button>
                              </div> 
                        </p>                        
                      </div>
                    </div>                    
                  </form> 
            </div>
            <div class="col-md-2"></div>
          </div>
       </div>
    </section>