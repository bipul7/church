<div id="append_div">
    <?php 
    $link_type = $sermon_file_info[0]['sermon_file_link_type'];
    $file_type = $sermon_file_info[0]['sermon_file_type'];
    $file = $sermon_file_info[0]['sermon_file']
    ?>

    <?php if($link_type=='local'){ ?>
    <video controls="controls" preload="metadata" width="360" height="280" style="margin: 0 auto; display: block;"><source src="<?php echo $this->config->item('sermon_file_source_path').$file ?>" type="video/mp4"></video>
    <?php } ?>

    <?php if($link_type=='global'){ ?> 

    <?php
        $url = $file;
        preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
        $id = $matches[1];
        $width = '360px';
        $height = '280px';
    ?>
    <iframe id="ytplayer" type="text/html" width="<?php echo $width ?>" height="<?php echo $height ?>"
        src="https://www.youtube.com/embed/<?php echo $id ?>?rel=0&showinfo=0&color=white&iv_load_policy=3"
        frameborder="0"></iframe>
    <?php } ?>
   
</div>



