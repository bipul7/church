

    <section id="page_title" class="gradintblue">  
      <div class="banner_img"><img src="<?= $this->config->item('public_asset_path') ?>img/banner.jpg" class="img-fluid"></div>
      <div class="banner_content">
        <div class="container text-center">
          <h1>Sermon Details</h1>
          <ul class="bradcumb">
            <li><a href="#">Home</a></li>
            <li> / </li>
            <li>Sermon</li>
            <li> / </li>
            <li><?php echo $title ?></li>
          </ul>
          
        </div>
      </div>  
    </section>
    
    <section id="main_contant">
      <div class="container">         
          <div class="row">
            <div class="col" id="leftside">

              <!-- main sermon details start -->
              <section id="main_contant">
      <div class="container">
           
          <div class="row">
            <div class="col">
              <div class="blog_list">
                <div class="blog_item">
                  <div class="blog_img"><img src="<?= $this->config->item('sermon_file_source_path').$sermon['sermon_feature_image'] ?>" class="img-fluid"></div>
                  <div id="attached_file">
                    <ul class="ss_social_staff" >
                    <?php 
                      //$video = !empty($sermon['sermon_video']['sermon_file']) ?  $this->config->item('sermon_file_source_path'). $sermon['sermon_video']['sermon_file'] : '#';
                      //$audio = !empty($sermon['sermon_audio']['sermon_file']) ?  $this->config->item('sermon_file_source_path'). $sermon['sermon_audio']['sermon_file'] : '#';
                      //$file = !empty($sermon['sermon_file']['sermon_file']) ?  $this->config->item('sermon_file_source_path'). $sermon['sermon_file']['sermon_file'] : '#';
                    ?>      
                    
                    <!-- <li><a href="<?= $audio ?>"><i class="fa fa-music"></i></a></li>       -->
                    
                    <?php if(isset($sermon['sermon_video']['sermon_file']) && !empty($sermon['sermon_video']['sermon_file']) ){ ?>
                      <li><a href="javascript:void(0)" class="open_modal" key="<?php echo $sermon['sermon_video']['sermon_file_key'] ?>" data-toggle="modal" data-target="#viewFile"><i class="fa fa-film"></i></a></li>
                    <?php } ?>

                    <?php if( isset($sermon['sermon_audio']['sermon_file']) && !empty($sermon['sermon_audio']['sermon_file']) ){ ?>
                      <li><a href="javascript:void(0)" class="open_modal" key="<?php echo $sermon['sermon_audio']['sermon_file_key'] ?>" data-toggle="modal" data-target="#viewFile"><i class="fa fa-music"></i></a></li> 
                    <?php } ?>

                  <?php if(isset($sermon['sermon_file']['sermon_file']) && !empty($sermon['sermon_file']['sermon_file']) ){ ?>
                    <li><a href="<?php echo $this->config->item('sermon_file_source_path'). $sermon['sermon_file']['sermon_file']  ?>" target="_blank"><i class="fa fa-file"></i></a></li>
                    <li><a href="<?php echo $this->config->item('sermon_file_source_path'). $sermon['sermon_file']['sermon_file']  ?>" download><i class="fa fa-download"></i></a></li>
                  <?php } ?>
                  </ul>

                  </div>
                  <h3><?= !empty($sermon['sermon_title']) ? $sermon['sermon_title'] : '' ?> </h3>
                  <div class="blog_p_date">
                    <h2>06</h2>
                    <p>jan</p>
                  </div>
                  <p><?= !empty($sermon['sermon_description']) ? $sermon['sermon_description'] : '' ?></p>
                 
                </div>
             
                 <hr>
                 <?php if(!empty($sermon['sermon_tags'])){ ?>
                 <div class="tag_post">
                  <h3 class="pd_title">Tags</h3>
                  <div class="row">
                    <div class="col">
                      <ul>
                        <?php foreach ($sermon['sermon_tags'] as $tag) { ?>
                        <li><a href="javascript:void(0)"><?php echo $tag['sermon_tag'] ?></a></li>    
                        <?php } ?>                    
                      </ul>
                    </div>
                  </div>
                 </div>
                 <?php } ?>
           
              </div>
            
            </div>
           
          </div>
          <div class="row older_post">
        <div class="col"> 
          
          <?php if (!empty($sermon['prev_sermon'])) { ?>
              <a href="<?= !empty($sermon['prev_sermon']['sermon_key']) ? base_url() . 'sermon/' . $sermon['prev_sermon']['sermon_key'] : '#' ?>"
                 class="btn btndarkborder  pull-left">
                  <?= !empty($sermon['prev_sermon']['sermon_title']) ? 'Previous' : 'Previous' ?>
              </a>
          <?php } ?>
          <?php if (!empty($sermon['next_sermon'])) { ?>
              <a href="<?= !empty($sermon['next_sermon']['sermon_key']) ? base_url() . 'sermon/' . $sermon['next_sermon']['sermon_key'] : '#' ?>"
                 class="btn btndark pull-right ">
                  <?= !empty($sermon['next_sermon']['sermon_title']) ? 'Next' : 'Next' ?>
              </a>
          <?php } ?>

        </div>
        
      </div>
        <!-- <div class="comment_box">
          <div class="row">
            <div class="col">
              <h3 class="pd_title">Comment</h3>
              <div class="panel panel-white post panel-shadow">
                <div class="post-heading">
                    <div class="pull-left image">
                        <img src="http://bootdey.com/img/Content/user_1.jpg" class="img-circle avatar" alt="user profile image">
                    </div>
                    <div class="pull-left meta">
                        <div class="title h5">
                            <a href="#"><b>Ryan Haywood</b></a>
                            made a post.
                        </div>
                        <h6 class="text-muted time">1 minute ago</h6>
                    </div>
                </div> 
                <div class="post-description"> 
                    <p>Bootdey is a gallery of free snippets resources templates and utilities for bootstrap css hmtl js framework. Codes for developers and web designers</p>
                    
                </div>
            </div>
            </div>
          </div>
        <form class="reply_comments">
          <div class="row">
            <div class="col">
                <br/>
                <div class="form-group">
                  <label for="comment">Reply:</label>
                  <textarea class="form-control" rows="5" id="comment"></textarea>
                </div>
              
            </div>
            <div class="col">
               <div class="form-group">
                 <br/> 
                <button class="btn btndarkborder"> Share </button>
              </div>
            </div>
          </div>
          </form>
        </div> -->
       </div>
    </section>
    <!-- main sermon details end -->
            </div>
            <div class="col-4" id="sidebar">
               

                <div class="recent_post">
                   <h3 class="sidebar_title">Recent Sermon</h3>
                   <?php foreach ($recent_sermons as $sermon) { 
                    $sermon_url = !empty($sermon['sermon_key']) ? base_url() . 'sermon/' . $sermon['sermon_key'] : '#';
                    ?>
                  <div class="row">
                    <div class="col-4"><img src="<?= $this->config->item('sermon_file_source_path').$sermon['sermon_feature_image'] ?>" class="img-fluid"></div>
                    <div class="col">
                      <h5><a href="<?= $sermon_url ?>">
                        <?php 
                        $count2 = strlen($sermon['sermon_title']);
                          if($count2>30){ $dot = "...";}
                          else{$dot='';}
                          $title = substr($sermon['sermon_title'], 0, 30);
                          echo $title.$dot;
                          ?>
                      </a></h5>
                      <p>
                      <?php 
                        $date=strtotime($sermon['sermon_created_at']);
                        $dateshow = date("M d Y",$date);
                        $timeshow = date("h m A", $date);
                        echo $dateshow.' | '.$timeshow;
                      ?>
                      </p>
                    </div>
                  </div>
                  <?php } ?>                 
                 
                </div>

                <div class="recent_post">
                   <h3 class="sidebar_title">Recent Post</h3>
                   <?php foreach ($recent_posts as $post) { 
                    $post_url = !empty($post['blog_key']) ? base_url() . 'blog/' . $post['blog_key'] : '#';
                    ?>
                  <div class="row">
                    <div class="col-4"><img src="<?= $this->config->item('blog_image_source_path').$post['blog_feature_image'] ?>" class="img-fluid"></div>
                    <div class="col">
                      <h5><a href="<?= $post_url ?>">
                        <?php 
                        $count2 = strlen($post['blog_title']);
                          if($count2>30){ $dot = "...";}
                          else{$dot='';}
                          $title = substr($post['blog_title'], 0, 30);
                          echo $title.$dot;
                          ?>
                      </a></h5>
                      <p>
                      <?php 
                        $date=strtotime($post['blog_created_at']);
                        $dateshow = date("M d Y",$date);
                        $timeshow = date("h m A", $date);
                        echo $dateshow.' | '.$timeshow;
                      ?>
                      </p>
                    </div>
                  </div>
                  <?php } ?>                   
                 
                </div>

            </div>
          </div>
 
       </div>
    </section>

    <div class="modal fade" id="viewFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="event_booking_form">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">View</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body ">
                    
                    <div id="file_play_div">
                       abcdefgh
                    </div>
                    
                </div>
                
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
   // edit blog
document.addEventListener("DOMContentLoaded", function () {
 $(document).on('click','.open_modal', function(){
         var file_key   = $(this).attr('key');
       $.ajax({
            url:'/render-sermon-file',
            method:'POST',
            data:{file_key:file_key},
            success:function(data){
             
               var result = JSON.parse(data);
               $('#file_play_div').html(result.file_play_div);
            }
        })
    });
 })
</script>
 
