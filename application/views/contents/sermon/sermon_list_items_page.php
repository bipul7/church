
<?php if (!empty($sermons)) { ?>
  <?php foreach ($sermons as $sermon) { ?>
   <?php 
      $url = !empty($sermon['sermon_key']) ? base_url() . 'sermon/' . $sermon['sermon_key'] : '#';      
    ?>

<div class="col-sm-3" style="border: 1px solid #F3F3F3; margin-bottom: 15px;">
  <div class="staff_item text-center">
    <div class="">
      <a href="<?= $url ?>"><img src="<?= $this->config->item('sermon_file_source_path').$sermon['sermon_feature_image'] ?>"  class="img-fluid"> </a>
    </div>
    
    <a href="<?= $url ?>"><h5><b><?php echo $sermon['sermon_title'] ?></b></h5></a>
    <h6><?php echo $sermon['staff_type_name']." : ";?><span> <a href="<?= base_url()?>staff/<?=$sermon['staff_key']?>"><?php echo $sermon['staff_name'] ?></a></h6>
      <p>
        <?php 
        if(!empty($sermon['sermon_description'])){
          $count = strlen($sermon['sermon_description']);
          if($count>50){ $more = "...";}
          else{$more='';}
          $sermon_description = substr($sermon['sermon_description'], 0, 50);
          echo strip_tags($sermon_description).$more;
        }
        ?>

      </p>
    <ul class="ss_social_staff">
      <?php 
        $video = !empty($sermon['sermon_video']) ?  $this->config->item('sermon_file_source_path'). $sermon['sermon_video']['sermon_file'] : '#';
        $audio = !empty($sermon['sermon_audio']) ?  $this->config->item('sermon_file_source_path'). $sermon['sermon_audio']['sermon_file'] : '#';
        $file = !empty($sermon['sermon_file']) ?  $this->config->item('sermon_file_source_path'). $sermon['sermon_file']['sermon_file'] : '#';
      ?>      
      <!-- <li><a href="javascript:void(0)" class="open_modal" key="" data-toggle="modal" data-target="#viewFile"><i class="fa fa-film"></i></a></li>
      <li><a href="<?= $audio ?>"><i class="fa fa-music"></i></a></li>      
      <li><a href="<?= $file ?>" target="_blank"><i class="fa fa-file"></i></a></li>
      <li><a href="<?= $file ?>" download><i class="fa fa-download"></i></a></li> -->
    </ul>
  </div>
</div>

  <?php } ?>
<?php } ?>



<!-- <div class="modal fade" id="viewFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <form class="">
                  <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">View</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body ">
                      
                      <div id="file_play_div">
                         
                      </div>
                      
                  </div>
                  
              </form>
          </div>
      </div>
  </div> -->

 

<!-- <script type="text/javascript">
   // edit blog
document.addEventListener("DOMContentLoaded", function () {
 $(document).on('click','.open_modal', function(){
         var file_key   = $(this).attr('key');
       $.ajax({
            url:'/render-sermon-file',
            method:'POST',
            data:{file_key:file_key},
            success:function(data){
             
               var result = JSON.parse(data);
               $('#file_play_div').html(result.file_play_div);
            }
        })
    });
 })
</script> -->

