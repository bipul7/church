  <div class="recent_post">
     <h3 class="sidebar_title">Recent Post</h3>
    
     <?php foreach ($recent_posts as $post) { 
      $post_url = !empty($post['blog_key']) ? base_url() . 'blog/' . $post['blog_key'] : '#';
      ?>
    <div class="row">
      <div class="col-4"><img src="<?= $this->config->item('blog_image_source_path').$post['blog_feature_image'] ?>" class="img-fluid"></div>
      <div class="col">
        <h5><a href="<?= $post_url ?>">
          <?php 
          $count2 = strlen($post['blog_title']);
            if($count2>30){ $dot = "...";}
            else{$dot='';}
            $title = substr($post['blog_title'], 0, 30);
            echo $title.$dot;
            ?>
        </a></h5>
        <p>
        <?php 
          $date=strtotime($post['blog_created_at']);
          $dateshow = date("M d Y",$date);
          $timeshow = date("h m A", $date);
          echo $dateshow.' | '.$timeshow;
        ?>
        </p>
      </div>
    </div>
    <hr/>
    <?php } ?>
    
    
  </div>