

<?php if (!empty($blogs)) { ?>
  <?php foreach ($blogs as $blog) { ?>
    <?php 
      $url = !empty($blog['blog_key']) ? base_url() . 'blog/' . $blog['blog_key'] : '#';      
    ?>

    <div class="blog_item">
        <div class="blog_img"><a href="<?= $url ?>"><img src="<?= $this->config->item('blog_image_source_path').$blog['blog_feature_image'] ?>" class="img-fluid"></a></div>
        <h3> <a href="<?= $url ?>"><?= !empty($blog['blog_title']) ? $blog['blog_title'] : '' ?></a> </h3>
        <div class="blog_p_date">
          <h2>
            <?= !empty($blog['blog_created_at']) ? date("d", strtotime($blog['blog_created_at'])) : 'Unavailable' ?>
          </h2>
          <p>
            <?= !empty($blog['blog_created_at']) ? date("M", strtotime($blog['blog_created_at'])) : 'Unavailable' ?>
          </p>
        </div>
        <p>
        <?php 
        if(!empty($blog['blog_description'])){
          $count = strlen($blog['blog_description']);
          if($count>200){ $more = "...";}
          else{$more='';}
          $blog_description = substr($blog['blog_description'], 0, 200);
          echo strip_tags($blog_description).$more;
        }
        ?>

      </p>
        <a href="<?= $url ?>" class="float-left coment_btn"> <i class="fa fa-comment-o"></i> <?= !empty($blog['blog_comments']) ? count($blog['blog_comments']) : '0' ?> Comment</a>
        <a href="<?= $url ?>" class="btn btndarkborder float-right">Read More</a>
    </div>

    <?php } ?>
<?php } ?>