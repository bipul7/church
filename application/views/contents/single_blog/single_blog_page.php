<section id="page_title" class="gradintblue">
    <div class="banner_img"><img src="<?= $this->config->item('public_asset_path') ?>img/banner.jpg" class="img-fluid">
    </div>
    <div class="banner_content">
        <div class="container text-center">
          <h1>Blog Details</h1>
          <ul class="bradcumb">
            <li><a href="/">Home</a></li>
            <li> / </li>
            <li><a href="<?= base_url()?>blogs">Blog</a></li>
            <li> / </li>
            <li> <?php echo $title ?> </li>
          </ul>
          
        </div>
      </div> 
</section>


<section id="main_contant">
      <div class="container">           
          <div class="row">
            <div class="col">
              <div class="blog_list">
                <div class="blog_item">
                  <div class="blog_img"><a href="#"><img src="<?= $this->config->item('blog_image_source_path').$blog['blog_feature_image'] ?>" class="img-fluid"></a></div>
                  <h3> <a href="#"><?= !empty($blog['blog_title']) ? $blog['blog_title'] : '' ?></a> </h3>
                  <div class="blog_p_date">
                    <h2>06</h2>
                    <p>jan</p>
                  </div>
                  <p>
                      <?= !empty($blog['blog_description']) ? $blog['blog_description'] : '' ?>
                  </p>
                </div>
             
                 <hr>
                 <?php if(!empty($blog['blog_tags'])){ ?>
                 <div class="tag_post">
                  <h3 class="pd_title">Tag Post</h3>
                  <div class="row">
                    <div class="col">
                      <ul>
                        <?php foreach ($blog['blog_tags'] as $tag) { ?>
                        <li><a href="#"><?php echo $tag['blog_tag'] ?></a></li>    
                        <?php } ?>                    
                      </ul>
                    </div>
                  </div>
                 </div>
                 <?php } ?>
           
              </div>
            
            </div>
           
          </div>
          <div class="row older_post">
        <div class="col"> 
          

          <!-- b code start -->

            <?php if (!empty($blog['prev_blog'])) { ?>
                <a href="<?= !empty($blog['prev_blog']['blog_key']) ? base_url() . 'blog/' . $blog['prev_blog']['blog_key'] : '#' ?>"
                   class="btn btndarkborder  pull-left">
                    <?= !empty($blog['prev_blog']['blog_title']) ? $blog['prev_blog']['blog_title'] : 'Previous' ?>
                </a>
            <?php } ?>
            <?php if (!empty($blog['next_blog'])) { ?>
                <a href="<?= !empty($blog['next_blog']['blog_key']) ? base_url() . 'blog/' . $blog['next_blog']['blog_key'] : '#' ?>"
                   class="btn btndark pull-right ">
                    <?= !empty($blog['next_blog']['blog_title']) ? $blog['next_blog']['blog_title'] : 'Next' ?>
                </a>
            <?php } ?>



          <!-- b code ent -->



        </div>
        
      </div>
        <div class="comment_box">
          <?php if(!empty($blog['blog_comments'])){ ?>
          <div class="row">
            <div class="col">
              <h3 class="pd_title">Comment</h3>
              <?php foreach ($blog['blog_comments'] as $comment) { ?>
              <div class="panel panel-white post panel-shadow">
                <div class="post-heading">
                    <div class="pull-left image">
                        <img src="<?php echo $this->config->item('default_user_image') ?>" class="img-circle avatar" alt="user profile image">
                    </div>

                    
                    <div class="pull-left meta">
                        <div class="title h6">
                            <a href="#"><b><?php echo $comment['blog_comment_username'] ?></b></a>
                        </div>
                        <h6 class="text-muted time">
                            <?php 
                              $date=strtotime($comment['blog_comment_created_at']);
                              $dateshow = date("M d Y",$date);
                              $timeshow = date("h m A", $date);
                              echo $dateshow.' | '.$timeshow;
                            ?>
                        </h6>
                    </div>


                </div> 
                <div class="post-description"> 
                    <p><?php echo $comment['blog_comment_usermessage'] ?></p>
                    <!-- <div class="stats">
                        <a href="#" class="btn btn-default stat-item">
                            <i class="fa fa-thumbs-up icon"></i>2
                        </a>
                        <a href="#" class="btn btn-default stat-item">
                            <i class="fa fa-thumbs-down icon"></i>12
                        </a>
                    </div> -->
                </div>
            </div><br>

            <?php } ?>
            </div>
          </div>
          <?php } ?>
        <form class="reply_comments">
          <div class="row">
            <div class="col">
                <br/>
                <div class="form-group">
                  <label for="comment">Reply:</label>
                  <textarea class="form-control" rows="5" id="comment"></textarea>
                </div>
              
            </div>
            <div class="col">
               <div class="form-group">
                 <br/> 
                <button class="btn btndarkborder"> Share </button>
              </div>
            </div>
          </div>
          </form>
        </div>
       </div>
    </section>