 <section id="page_title" class="gradintblue">  
      <div class="banner_img">
       

        <?php 
          if(!empty($page_contents['page_header_image'])){ ?>
             <img src="<?php echo $this->config->item('page_image_source_path').$page_contents['page_header_image']; ?>" class="img-fluid" style="width: 100%;">
        <?php } ?>

        <?php 
          if(empty($page_contents['page_header_image'])){ ?>
            <img src="<?= $this->config->item('public_asset_path') ?>img/banner.jpg" class="img-fluid">
        <?php } ?>

      </div>
      <div class="banner_content">
        <div class="container text-center">
          <h1><?php if(isset($page_contents['page_title'])){ echo $page_contents['page_title']; } ?></h1>
          <ul class="bradcumb">
            <li><a href="/">Home</a></li>
            <li> / </li>
            <li><?php if(isset($page_contents['page_title'])){ echo $page_contents['page_title']; } ?></li>
          </ul>
          
        </div>
      </div>  
    </section>
    
     <section id="main_contant">
      <div class="container"> 
          <div class="row">
           
            <div class="col-md-12">
               <!-- success and error msg showing area -->
                <div class="row">                
                        <div class="col-md-12"> 
                        <?php if($this->session->userdata('error_msg')){ ?>
                        <div class="alert alert-danger">                    
                            <span><?=$this->session->userdata('error_msg');?></span>
                  <a style="float: right;" href="#" class="close" data-dismiss="alert">&times;</a>
                        </div>
                         <?php } $this->session->unset_userdata('error_msg');?>
                        <?php if($this->session->userdata('success_msg')){ ?>
                        <div class="alert alert-success">                    
                            <span><?=$this->session->userdata('success_msg');?></span>
                  <a style="float: right;" href="#" class="close" data-dismiss="alert">&times;</a>
                        </div>
                         <?php } $this->session->unset_userdata('success_msg'); ?>
                        </div>                
                </div>
              <!-- success and error msg showing area end-->
              <?php if(isset($page_contents)){ echo $page_contents['page_content']; } ?>
                   
            </div>
          </div>
       </div>
    </section>