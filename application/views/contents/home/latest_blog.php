<section id="latest_blog">
    <div class="container">
        <div class="site_heading pad50">
            <h2>BLOG</h2>
            <p class="small_titel">Our Church Blog Post</p>
        </div>
        <div class="blog_item">
            <div class="row">
                <div class="col">
                    <div class="gradintwhite">
                        <img src="<?= $this->config->item('blog_image_source_path').$latest_blog[0]['blog_feature_image'] ?>" class="img-fluid"></div>
                </div>
                <div class="col">
                    <div class="b_latest_cont">
                        <h3><?php echo $latest_blog[0]['blog_title'] ?></h3>
                        <p> -- 
                            <?php
                                $date = date('F d, Y',strtotime($latest_blog[0]['blog_created_at']));
                                echo $date; 
                            ?>
                        </p>
                        <p>
                            <?php 
                            if(!empty($latest_blog[0]['blog_description'])){
                              $count = strlen($latest_blog[0]['blog_description']);
                              if($count>200){ $more = "...";}
                              else{$more='';}
                              $blog_description = substr($latest_blog[0]['blog_description'], 0, 200);
                              echo strip_tags($blog_description).$more;
                            }
                            ?>
                        </p>

                        <?php 
                          $url = !empty($latest_blog[0]['blog_key']) ? base_url() . 'blog/' . $latest_blog[0]['blog_key'] : 'javascript:void(0)';      
                        ?>

                        <a href="<?= $url ?>" class="btn btndarkborder">Read more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>