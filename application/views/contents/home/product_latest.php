<section id="product_latest">
    <div class="container">
        <div class="site_heading pad50">

            <h2>SHOP</h2>
            <p class="small_titel">See Our Shop</p>
        </div>

        <div class="row">
            <div class="col">
                <div class="product_item gradintwhite">
                    <img src="assets/img/p1.jpg" class="img-fluid">
                    <div class="produt_l_cot">
                        <h5><a href="#">Inceptos orci hac libero</a></h5>
                        <p>
                            <del>$ 350.00</del>
                            $ 250.00
                        </p>
                    </div>
                    <button><i class="fa fa-shopping-cart"></i></button>
                </div>
            </div>
            <div class="col">
                <div class="product_item gradintwhite">
                    <img src="assets/img/p2.jpg" class="img-fluid">
                    <div class="produt_l_cot">
                        <h5><a href="#">Inceptos orci hac libero</a></h5>
                        <p>
                            <del>$ 350.00</del>
                            $ 250.00
                        </p>
                    </div>
                    <button><i class="fa fa-shopping-cart"></i></button>
                </div>
            </div>
            <div class="col">
                <div class="product_item gradintwhite">
                    <img src="assets/img/p3.jpg" class="img-fluid">
                    <div class="produt_l_cot">
                        <h5><a href="#">Inceptos orci hac libero</a></h5>
                        <p>
                            <del>$ 350.00</del>
                            $ 250.00
                        </p>
                    </div>
                    <button><i class="fa fa-shopping-cart"></i></button>
                </div>
            </div>
            <div class="col">
                <div class="product_item gradintwhite">
                    <img src="assets/img/p4.jpg" class="img-fluid">
                    <div class="produt_l_cot">
                        <h5><a href="#">Inceptos orci hac libero</a></h5>
                        <p>
                            <del>$ 350.00</del>
                            $ 250.00
                        </p>
                    </div>
                    <button><i class="fa fa-shopping-cart"></i></button>
                </div>
            </div>
        </div>
    </div>

</section>