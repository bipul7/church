<?php $icon = array();

$icons['facebook'] = "fa-facebook";
$icons['twitter'] = "fa-twitter";
$icons['google'] = "fa-google-plus";
$icons['youtube'] = "fa-youtube-play";
?>

<section id="our_staff">
    <div class="container">
        <div class="site_heading pad50">
            <h2>OUR STAFF</h2>
            <p class="small_titel">The churches must learn humility as well as teach it</p>
        </div>
        <div class="ss_our_staffs">
            <?php if (!empty($staffs)) { ?>
                <?php foreach ($staffs as $staff) { ?>
                    <div class="col">
                        <div class="staff_item text-center">
                            <div class="gradintwhitere">
                                <img src="<?= $staff['featured_image']['staff_image_name_with_path'] ?>"
                                     class="img-fluid">
                            </div>

                            <p><?= $staff['staff_name'] ?></p>
                            <p class="small_titel"><?= !empty($staff['staff_type'])?$staff['staff_type']['staff_type_name']:"" ?></p>

                            <?php if (!empty($staff['staff_social_links'])) { ?>
                                <ul class="ss_social_staff">
                                    <?php foreach ($staff['staff_social_links'] as $a_staff_social_link) { ?>
                                        <?php $icon = "";
                                        if (array_key_exists($a_staff_social_link['staff_social_link_name'], $icons)) {
                                            $icon = $icons[$a_staff_social_link['staff_social_link_name']];
                                        }
                                        ?>
                                        <li>
                                            <a href="//<?= $a_staff_social_link['staff_social_link_url'] ?>">
                                                <i class="fa <?= $icon ?>"></i>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>

                        </div>
                    </div>
                <?php } ?>
            <?php } ?>

        </div>
    </div>
</section>