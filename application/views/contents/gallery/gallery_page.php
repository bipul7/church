<section id="page_title" class="gradintblue">
    <div class="banner_img"><img src="<?= $this->config->item('public_asset_path') ?>img/banner.jpg" class="img-fluid">
    </div>
    <div class="banner_content">
        <div class="container text-center">
            <h1>Photo Gallery</h1>
            <ul class="bradcumb">
                <li><a href="/">Home</a></li>
                <li> /</li>
                <li>Gallery</li>
            </ul>

        </div>
    </div>
</section>
<style>
    .ajax-load {
        background: #e1e1e1;
        padding: 10px 0px;
        width: 100%;
    }
</style>

<section id="main_contant">
    <div class="container">
        <div class="site_heading pad50 gallery-tab-bar-mr">
            <button type="button" gallery-category-key="" class="gallery-cat-btn btn btndark ?>">
                All
            </button>
            <?php if (!empty($gallery_categories)) { ?>
                <?php foreach ($gallery_categories as $gallery_category) { ?>
                    <button type="button" gallery-category-key="<?= $gallery_category['gallery_category_key'] ?>"
                            class="gallery-cat-btn btn btndarkborder">
                        <?= $gallery_category['gallery_category_name'] ?>
                    </button>
                <?php } ?>
            <?php } ?>
        </div>


        <div id="gg-screen"></div>
        <div class="gg-box gallery_items" base-url="<?= base_url() ?>">
        </div>

        <div class="ajax-load text-center" style="display:none">
            <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More post</p>
        </div>


    </div>
</section>

<script type="text/javascript" src="<?= $this->config->item('public_custom_js_path')  ?>gallery_items.js"></script>