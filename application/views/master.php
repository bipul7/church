<!doctype html>
<base href="base_url()">
<html lang="en">
<head>
    <!-- Required meta tags -->
    <?= $meta ?>
    <title><?= isset($title) ? $title : ':: Church ::' ?>.</title>
    <!-- Bootstrap CSS -->
    <?= $headlink ?>

</head>
<body>
<header>
    <?= $navbar ?>
</header>

<?= $content ?>

<?= $footer ?>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<?= $footlink ?>

<script>
    new WOW().init();
</script>






</body>
</html>