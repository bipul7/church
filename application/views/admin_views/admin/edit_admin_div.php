<form id="editAdmin"  role="form" action="admin/update-admin" method="POST" enctype="multipart/form-data">
                        <div class="box-body">

                        <div class="row">
                            <div class="col-md-8">                            

                          <div class="form-group">
                             <label for="image_name" class="col-form-label">Image<span class="text-danger"> </span></label>
                                    <div class="dropzone" id="edit_dropzone">
                                        <div class="dz-message" >
                                            <h3> Click Here to select images</h3>
                                        </div>
                                    </div>
                          </div>

                           <div class="previews2" id="preview2"></div>

                            </div>

                            <div class="col-md-4" style="margin-top: 5%;">
                              <?php if(!empty($adminInfo[0]['image'])){ ?>
                              <img src="<?php echo $this->config->item('admin_source_path').$adminInfo[0]['image']; ?>" style="height: 100px; width: 100px;">
                              <?php } ?>
                            </div>
                          </div>                          

                          <div class="form-group">
                            <label for="email" class="col-4 col-form-label">Email<span class="text-danger">*</span></label>
                            <input type="text" required name="email" class="form-control" placeholder="Enter Email" value="<?php echo $adminInfo[0]['email']; ?>">
                          </div>

                          <div class="form-group">
                            <label for="username" class="col-4 col-form-label">Username<span class="text-danger">*</span></label>
                            <input type="text" required name="username" class="form-control" placeholder="Enter Username" value="<?php echo $adminInfo[0]['username']; ?>">
                            
                            <input type="hidden" required name="id" class="form-control" placeholder="Enter Username" value="<?php echo $adminInfo[0]['id']; ?>">
                          </div>

                          <div class="form-group">
                            <label for="password" class="col-4 col-form-label">Password<span class="text-danger"></span></label>
                            <input type="password" name="password" class="form-control"
                                                placeholder="******" >
                          </div>
                                               
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                      <script  type="text/javascript" src="custom-admin-javascript/admin/admin_edit.js"></script>