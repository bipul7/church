

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrators
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Administrator</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header">
           <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default"> Add Administrator</button>
          </div>

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  
                  <th>SL</th>
                  <th>Photo</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
                </thead>
               <tbody>
                    <?php $sl=0; foreach ($all_admin as $admin) { $sl++;?>
                   <tr id="<?php echo $admin['admin_key']; ?>">                      

                        <td><?php echo $sl; ?></td>
                        <td>
                           <?php 
                              if(!empty($admin['image'])){ ?>                                  
                                   <img src="<?php echo $this->config->item('admin_source_path').$admin['image']; ?>" style="height: 40px; width: 40px;"> 
                             <?php } ?>
                        </td>                       
                        <td id="username"><?php echo $admin['username']; ?></td>
                        <td id="email"><?php echo $admin['email']; ?></td>
                        <td class="modalOpen"> 
                            <a data-toggle="modal" class="edit_admin" data-target="#exampleModal2" href="javascript:void(0)"><i style="margin-right:10px; font-size: 16px;" data-toggle="tooltip" title="Edit" class="fa fa-pencil"></i></a>

                            <a href="javascript:void(0)"><i data-id1="<?php echo $admin['admin_key']; ?>" style="font-size: 16px;" data-toggle="tooltip" title="Delete" class="fa fa-trash btn_delete_admin"></i></a>
                        </td>
                    </tr>
                   <?php } ?> 
                   
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->


            <!-- add modal start -->

            <div class="modal fade" id="modal-default">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Administrator</h4>
                  </div>
                  <div class="modal-body">
                     <form id="addAdmin"  role="form" action="admin/all-admin" method="POST" enctype="multipart/form-data">
                        <div class="box-body">

                          <div class="form-group">
                             <label for="image_name" class="col-form-label">Image<span class="text-danger"></span></label>
                                    <div class="dropzone" >
                                            <div class="dz-message" >
                                                <h3> Click Here to select images</h3>
                                            </div>
                                    </div>
                          </div>
                          <div class="previews" id="preview"></div>
                          

                          <div class="form-group">
                            <label for="email" class="col-4 col-form-label">Email<span class="text-danger">*</span></label>
                            <input type="text" required name="email" class="form-control" placeholder="Enter Email">
                          </div>

                          <div class="form-group">
                            <label for="username" class="col-4 col-form-label">Username<span class="text-danger">*</span></label>
                            <input type="text" required name="username" class="form-control" placeholder="Enter Username">
                          </div>

                          <div class="form-group">
                            <label for="password" class="col-4 col-form-label">Password<span class="text-danger">*</span></label>
                            <input type="password" required name="password" class="form-control"
                                                placeholder="Enter Password">
                          </div>
                                               
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->


          <!-- edit modal start -->

            <div class="modal fade" id="exampleModal2">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit admin</h4>
                  </div>
                  <div class="modal-body">
                       <div id="edit_admin_div">
                       </div>
                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->





          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->

  <script>
  $(function () {
    $('#example1').DataTable()
   
  })
</script>










  
