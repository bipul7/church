
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <!-- <small>Control panel</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard active"></i> Dashboard</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-2">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $total_user; ?></h3>

              <p>Total User</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-person-add"></i> -->
            </div>
            <span class="small-box-footer"></span>
          </div>
        </div>

        <div class="col-md-2">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $total_sermon ?></h3>

              <p>Total Sermons</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-bag"></i> -->
            </div>
             <span class="small-box-footer"></span>
          </div>
        </div>

        <!-- ./col -->
        <div class="col-md-2">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $total_blog ?></h3>

              <p>Total Blog</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-stats-bars"></i> -->
            </div>
             <span class="small-box-footer"></span>
          </div>
        </div>
       
        <!-- ./col -->
        <div class="col-md-2">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $total_blog ?></h3>
              <p>Total Events</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-pie-graph"></i> -->
            </div>
            <span class="small-box-footer"></span>
          </div>
        </div>
        <!-- ./col -->

        <div class="col-md-2">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $total_booking; ?></h3>

              <p>Total Booking</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-person-add"></i> -->
            </div>
             <span class="small-box-footer"></span>
          </div>
        </div>

        <div class="col-md-2">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $today_booking; ?></h3>

              <p>Today Booking</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-stats-bars"></i> -->
            </div>
             <span class="small-box-footer"></span>
          </div>
        </div>

      </div>
      <!-- /.row -->


      <div class="row">
        <div id="chartContainer" style=""></div>
      </div>

      <!-- Main row -->
      
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"> </script>
    <script type="text/javascript">
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
  theme: "light1", // "light2", "dark1", "dark2"
  animationEnabled: false, // change to true    
  title:{
    text: "Last 7 Days Event Booking Report"
  },
  data: [
  {
    // Change type to "bar", "area", "spline", "pie",etc.
    type: "column",
    dataPoints: [
      { label: "apple",  y: 10  },
      { label: "orange", y: 15  },
      { label: "banana", y: 25  },
      { label: "mango",  y: 30  },
      { label: "grape",  y: 28  },
      { label: "aaa",    y: 28  },
      { label: "bbb",    y: 28  }
    ]
  }
  ]
});
//chart.render();

}
</script>
  