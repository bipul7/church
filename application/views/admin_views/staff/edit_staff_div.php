<style>
.dropzone2.dz-clickable {
    cursor: pointer;
}
.dropzone2 {
    min-height: 120px;
    border: 2px solid rgba(0,0,0,0.3);
    background: white;
    padding: 20px 20px;
}
.dropzone2, .dropzone2 * {
    box-sizing: border-box;
}

</style>
<form id="editStaff"  role="form" action="admin/update-staff" method="POST" enctype="multipart/form-data">
    <div class="box-body">    

         <div class="row">
          <div class="col-md-6">
           <div class="form-group">
            <label for="staff_name" class="col-4 col-form-label">Name<span class="text-danger">*</span></label>
            <input type="text" name="staff_name" class="form-control" value="<?php echo $staff_info[0]['staff_name'] ?>" placeholder="Enter Name">
          </div>
          </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="staff_type_id" class="col-4 col-form-label">Type<span class="text-danger">*</span></label>
                  <select required="required" name ="staff_type_id" class="form-control select2" style="width: 100%;">
                      <option value="">---</option>
                      <?php foreach ($view_staff_type as $type) { ?>
                      <option value="<?php echo $type['staff_type_id'] ?>" <?php if($type['staff_type_id']==$staff_info[0]['staff_type_id']){echo "selected";} ?>><?php echo $type['staff_type_name'] ?></option>
                      <?php } ?>
                    </select>
                </div>                                               
              </div>                            
          </div>

         <div class="row">
              <div class="col-md-6">
               <div class="form-group">
                <label for="email" class="col-4 col-form-label">Email<span class="text-danger">*</span></label>
                <input type="email" disabled="disabled" name="email" class="form-control"
                                    placeholder="Enter Email" value="<?php echo $staff_info[0]['email'] ?>">
              </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="password" class="col-4 col-form-label">Password<span class="text-danger"></span></label>
                  <input type="password" name="password" class="form-control" placeholder="******">
                </div>                                                 
              </div>                            
          </div>

          <div class="row">
              <div class="col-md-6">
               <div class="form-group">
                <label for="staff_order" class="col-4 col-form-label">Order<span class="text-danger">*</span></label>
                <input type="number" min="0" name="staff_order" class="form-control" value="<?php echo $staff_info[0]['staff_order'] ?>" placeholder="Enter Order (Ex: 0)">

                <input type="hidden" name="staff_id" class="form-control" value="<?php echo $staff_info[0]['staff_id'] ?>">
              </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="is_staff_active" class="col-4 col-form-label">Status<span class="text-danger">*</span></label>
                  <select required="required" name ="is_staff_active" class="form-control select2" style="width: 100%;">
                      <option value="1" <?php if($staff_info[0]['is_staff_active']==1){echo "selected";} ?>>Active</option>
                      <option value="0" <?php if($staff_info[0]['is_staff_active']==0){echo "selected";} ?>>Inactive</option>
                    </select>
                </div>                                              
              </div>                            
          </div>

          <div class="row">
              <div class="col-md-6">
                 <div class="form-group">
                  <label for="facebook" class="col-4 col-form-label">Facebook URL<span class="text-danger"></span></label>
                  <input type="text" name="facebook" class="form-control" placeholder="Facebook URL" value="<?php  if (!empty($facebook_url)) {echo $facebook_url[0]['staff_social_link_url']; } ?>">
                 </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="twitter" class="col-4 col-form-label">Twitter URL<span class="text-danger"></span></label>
                  <input type="text" name="twitter" class="form-control" placeholder="Twitter URL" value="<?php  if (!empty($twitter_url)) {echo $twitter_url[0]['staff_social_link_url']; } ?>">
                 </div>                                              
              </div>                            
          </div>

          <div class="row">
              <div class="col-md-6">
                 <div class="form-group">
                  <label for="google" class="col-4 col-form-label">Google URL<span class="text-danger"></span></label>
                  <input type="text" name="google" class="form-control" placeholder="Google URL" value="<?php  if (!empty($google_url)) {echo $google_url[0]['staff_social_link_url']; } ?>">
                 </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="youtube" class="col-4 col-form-label">Youtube URL<span class="text-danger"></span></label>
                  <input type="text" name="youtube" class="form-control" placeholder="Youtube URL" value="<?php  if (!empty($youtube_url)) {echo $youtube_url[0]['staff_social_link_url']; } ?>">
                 </div>                                              
              </div>                            
          </div>

          <div class="row">
              <div class="col-md-6">
                   <div class="form-group">
                     <label for="image_name" class="col-form-label">Image<span class="text-danger">*</span></label>
                          <div class="dropzone" id="edit_dropzone">
                              <div class="dz-message" >
                                  <h3> Click Here to select images</h3>
                              </div>
                          </div>
                  </div>
                   <div class="previews2" id="preview2"></div>

                    <?php foreach ($view_staff_image as $image) { ?>

                    <span id="<?php echo $image['staff_image_id']; ?>" class="editStaffImgDiv" style="margin-right: 10px;">
                      <input type="hidden" name="image2[]" value="<?php echo $image['staff_image_name']; ?>" > 

                      <img class="card-img-top img-fluid footage_thumb" style="height:60px; width:60px;" src="<?php echo $this->config->item('staff_source_path').$image['staff_image_name']; ?>"  alt="Staff Image">

                      <input type="radio" id="staff_feature_image" name="staff_feature_image" value="<?php echo $image['staff_image_name']; ?>" data-toggle="tooltip" title="Set Feature Image" <?php if($image['is_staff_image_featured']=='1'){echo "checked";} ?> data-parsley-multiple="add_user">
                      
                      <span><a href="void:javascript(0);" data-toggle="tooltip" title="Delete Image" data-staff_image_id="<?php echo $image['staff_image_id']; ?>" class="btn_delete_staff_image">X</a></span>
                    </span>
                    <?php } ?>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="staff_description" class="col-4 col-form-label">Description</label>
                  <textarea type="text" name="staff_description" class="form-control" placeholder="Enter Description" rows="7"><?php echo $staff_info[0]['staff_description'] ?></textarea>
                </div>                                                
              </div>                            
          </div>






      
      

                               

      

      

        

      <hr><hr>


      
                           
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>

  <script  type="text/javascript" src="custom-admin-javascript/staff/staff_edit.js"></script>





