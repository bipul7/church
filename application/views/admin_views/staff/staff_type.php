
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Staff Type
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Staff Type</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header">
           <?php $permission = $this->permission->hasAccess(array('add_staff_type','view_staff_type','edit_staff_type','delete_staff_type')); ?>
          <?php if($permission['add_staff_type']==1){ ?> 
           <a href="admin/add-staff-type" class="btn btn-success" > Add Staff Type</a>
           <?php } ?>
          </div>

            <?php if($permission['view_staff_type']==1){ ?> 
            <!-- /.box-header -->
            <div class="box-body">
              <table id="staff_type_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                 <thead>
                   <tr>                  
                      <th>SL</th>
                      <th>Role Name</th>
                      <th>Action</th>
                    </tr>
                 </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            <?php } ?>

            <!-- /.modal -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->

  <script>
  $(function () {
    $('#example1').DataTable()
   
  })
</script>










  
