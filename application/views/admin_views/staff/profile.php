<?php //print_r($admin_info); die; ?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profile
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        

            <!-- /.box-header -->
          <form id="profile"  role="form" action="admin/profile" method="POST" enctype="multipart/form-data">  
            <div class="box-body">
              <div class="row">
                  <div class="col-md-6"> 
                   <div class="form-group">
                      <label for="email" class="col-4 col-form-label">Email<span class="text-danger">*</span></label>
                      <input type="email" name="email" disabled="disabled" class="form-control" placeholder="Enter Email" value="<?php echo $staff_info[0]['email'] ?>">
                    </div> 
                  </div>
                  <div class="col-md-6"></div>                            
              </div>

              <div class="row">
                  <div class="col-md-6">
                   <div class="form-group">
                    <label for="name" class="col-4 col-form-label">Name<span class="text-danger">*</span></label>
                    <input type="text" name="staff_name" class="form-control"
                                        placeholder="Enter Email" value="<?php echo $staff_info[0]['staff_name'] ?>">
                  </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                    <label for="name" class="col-4 col-form-label">Password<span class="text-danger"></span></label>
                    <input type="text" name="password" class="form-control"
                                        placeholder="******">
                  </div>                                                
                  </div>                            
            </div>

            <div class="row">
              <div class="col-md-6">
                 <div class="form-group">
                  <label for="facebook" class="col-4 col-form-label">Facebook URL<span class="text-danger"></span></label>
                  <input type="text" name="facebook" class="form-control" placeholder="Facebook URL" value="<?php  if (!empty($facebook_url)) {echo $facebook_url[0]['staff_social_link_url']; } ?>">
                 </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="twitter" class="col-4 col-form-label">Twitter URL<span class="text-danger"></span></label>
                  <input type="text" name="twitter" class="form-control" placeholder="Twitter URL" value="<?php  if (!empty($twitter_url)) {echo $twitter_url[0]['staff_social_link_url']; } ?>">
                 </div>                                              
              </div>                            
          </div>

          <div class="row">
              <div class="col-md-6">
                 <div class="form-group">
                  <label for="google" class="col-4 col-form-label">Google URL<span class="text-danger"></span></label>
                  <input type="text" name="google" class="form-control" placeholder="Google URL" value="<?php  if (!empty($google_url)) {echo $google_url[0]['staff_social_link_url']; } ?>">
                 </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="youtube" class="col-4 col-form-label">Youtube URL<span class="text-danger"></span></label>
                  <input type="text" name="youtube" class="form-control" placeholder="Youtube URL" value="<?php  if (!empty($youtube_url)) {echo $youtube_url[0]['staff_social_link_url']; } ?>">
                 </div>                                              
              </div>                            
          </div>
              
              <div class="row">
                <div class="col-md-6">                  
                        <div class="box-body">
                          <div class="form-group">
                             <label for="image_name" class="col-form-label">Image<span class="text-danger"></span></label>
                                  <div class="dropzone" id="edit_dropzone">
                                      <div class="dz-message" >
                                          <h3> Click Here to select images</h3>
                                      </div>
                                  </div>
                          </div>
                           <div class="previews2" id="preview2"></div>

                          <!-- image view area -->
                             <?php foreach ($view_staff_image as $image) { ?>
                              <span id="<?php echo $image['staff_image_id']; ?>" class="editStaffImgDiv" style="margin-right: 10px;">
                                <input type="hidden" name="image2[]" value="<?php echo $image['staff_image_name']; ?>" > 

                                <img class="card-img-top img-fluid footage_thumb" style="height:60px; width:60px;" src="<?php echo $this->config->item('staff_source_path').$image['staff_image_name']; ?>"  alt="Staff Image">

                                <input type="radio" id="staff_feature_image" name="staff_feature_image" value="<?php echo $image['staff_image_name']; ?>" data-toggle="tooltip" title="Set Feature Image" <?php if($image['is_staff_image_featured']=='1'){echo "checked";} ?> data-parsley-multiple="add_user">
                                
                                <span><a href="void:javascript(0);" data-toggle="tooltip" title="Delete Image" data-staff_image_id="<?php echo $image['staff_image_id']; ?>" class="btn_delete_staff_image">X</a></span>
                              </span>
                              <?php } ?>
                          <!-- image view area end -->

                           <input type="hidden" name="staff_id" class="form-control"
                                                value="<?php echo $staff_info[0]['staff_id']; ?>">
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                  <label for="staff_description" class="col-4 col-form-label">Description</label>
                  <textarea type="text" name="staff_description" class="form-control" placeholder="Enter Description" rows="7"><?php echo $staff_info[0]['staff_description'] ?></textarea>
                </div>    
                  
                </div>
              </div>

            </div>
           </form>
            <!-- /.box-body -->
            
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->

  <script>
  $(function () {
    $('#example1').DataTable()
   
  })
</script>










  
