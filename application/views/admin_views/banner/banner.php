<style>
  .delete{
  color:white;
  background-color:rgb(231, 76, 60);
  text-align:center;
  margin-top:2px;
  font-weight:700;
  border-radius:5px;
  min-width:20px;
  cursor:pointer;
}

.add-one{
  color:green;
  font-weigth:bolder;
  cursor:pointer;
  margin-top:10px;
}

.add-one2{
  color:green;
  font-weigth:bolder;
  cursor:pointer;
  margin-top:10px;
}


</style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Banner
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Banner</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header">
          <?php $permission = $this->permission->hasAccess(array('add_banner','view_banner','edit_banner','delete_banner')); ?>

          <?php if($permission['add_banner']==1){ ?>          
           <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_banner"> Add Image </button>
           <?php } ?>
          </div>

          <?php if($permission['view_banner']==1){ ?>
            <!-- /.box-header -->
            <div class="box-body">
               <table id="banner" class="table table-striped table-bordered" cellspacing="0" width="100%">
                 <thead>
                  <tr>
                    <th>SL</th>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Action</th>
                  </tr>
                 </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            <?php } ?>


            <?php if($permission['add_banner']==1){ ?>
            <!-- add modal start -->
            <div class="modal fade" id="add_banner">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Banner Image</h4>
                  </div>
                  <div class="modal-body">
                     <form id="addSlideshow"  role="form" action="admin/slideshow" method="POST" enctype="multipart/form-data">
                        <div class="box-body">                                         

                          <div class="form-group">
                             <label for="image_name" class="col-form-label">Image<span class="text-danger">*</span></label>
                                    <div class="dropzone" >
                                            <div class="dz-message" >
                                                <h3> Click Here to select images</h3>
                                            </div>
                                    </div>
                          </div>

                           <div class="previews" id="preview"></div>

                          <div class="form-group">
                            <label for="banner_image_title" class="col-4 col-form-label">Title<span class="text-danger">*</span></label>
                            <input type="text" name="banner_image_title" class="form-control"
                                                placeholder="Enter Title">
                          </div>

                          <div class="form-group">
                            <label for="banner_image_subtitle" class="col-4 col-form-label">Sub Title<span class="text-danger">*</span></label>
                            <input type="text" name="banner_image_subtitle" class="form-control" placeholder="Enter Subtitle">
                          </div>

                          <div class="form-group">
                            <label for="banner_image_description" class="col-4 col-form-label">Image Description</label>
                            <input type="text" name="banner_image_description" class="form-control" placeholder="Enter Description">
                          </div>                          

                          <div class="form-group">
                            <label for="banner_order" class="col-4 col-form-label">Order<span class="text-danger">*</span></label>
                            <input type="number" min="0" name="banner_order" class="form-control" placeholder="Enter Order (Ex: 0)">
                          </div><hr><hr>

                          <!-- banner anchor area start -->

                          <div class="dynamic-element form-groupp" style="display: none;">
                            <div class="row">
                              <div class="col-md-4">
                                <div class="form-group">                                  
                                  <input type="text" name="banner_anchor_text[]" class="form-control" placeholder="Anchor Text">
                                </div>
                              </div>

                              <div class="col-md-4">
                                <div class="form-group">                                  
                                  <input type="text" name="banner_anchor_url[]" class="form-control" placeholder="Anchor URL">
                                </div>
                              </div>

                              <div class="col-md-3">
                                <div class="form-group">                                  
                                  <input type="text" name="banner_anchor_class[]" class="form-control" placeholder="Anchor Class">
                                </div>
                              </div>

                              <div class="col-md-1">
                                <p class="delete">x</p>
                              </div>
                          </div>
                          </div>

                          <div class="dynamic-anchor">
                            <!-- Dynamic element will be cloned here -->
                            <!-- You can call clone function once if you want it to show it a first element-->
                          </div>

                          <div class="col-md-12">
                            <p class="add-one">+ Add Banner Anchor</p>
                          </div>

                          <!-- banner anchor area end -->                          
                                               
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <?php } ?>


            <?php if($permission['edit_banner']==1){ ?>
            <!-- edit modal start -->
            <div class="modal fade" id="edit_banner">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Banner</h4>
                  </div>
                  <div class="modal-body">
                       <div id="edit_banner_div">
                       </div>
                  </div>                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <?php } ?>





          </div>
          <!-- /.box -->

    </section>
    <!-- /.content --> 
