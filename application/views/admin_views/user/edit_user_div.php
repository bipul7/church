<form id="editUser"  role="form" action="admin/update-user" method="POST" enctype="multipart/form-data">
        <div class="box-body">
          <div class="row">
                            <div class="col-md-8">                            

                          <div class="form-group">
                             <label for="image_name" class="col-form-label">Image<span class="text-danger"> </span></label>
                                    <div class="dropzone" id="edit_dropzone">
                                        <div class="dz-message" >
                                            <h3> Click Here to select images</h3>
                                        </div>
                                    </div>
                          </div>

                           <div class="previews2" id="preview2"></div>

                            </div>

                            <div class="col-md-4" style="margin-top: 5%;">
                              <?php if(!empty($userInfo[0]['user_image'])){ ?>
                              <img src="<?php echo $this->config->item('user_source_path').$userInfo[0]['user_image']; ?>" style="height: 100px; width: 100px;">
                              <?php } ?>
                            </div>
                          </div>
          

          <div class="form-group">
            <label for="user_email" class="col-4 col-form-label">Email<span class="text-danger">*</span></label>
            <input type="email" required name="user_email" class="form-control" placeholder="Enter Email" value="<?php echo $userInfo[0]['user_email']; ?>">
          </div>

          <input type="hidden" required name="user_id" class="form-control" value="<?php echo $userInfo[0]['user_id']; ?>">

          <div class="form-group">
            <label for="user_username" class="col-4 col-form-label">Username<span class="text-danger">*</span></label>
            <input type="text" required name="user_username" class="form-control" placeholder="Enter Username" value="<?php echo $userInfo[0]['user_username']; ?>">
          </div>

           <div class="form-group">
              <label for="user_role" class="col-4 col-form-label">User Role<span class="text-danger">*</span></label>
              
              <select required="required" name ="user_role" class="form-control select2" style="width: 100%;">
                  <option value="">---</option>
                  <?php foreach ($all_role as $role) { ?>
                  <option value="<?php echo $role['user_role_id'] ?>" <?php if($role['user_role_id']==$userInfo[0]['user_role']){echo "selected";} ?>><?php echo $role['user_role_name'] ?></option>
                  <?php } ?>
                </select>

            </div>

          <div class="form-group">
            <label for="user_phone" class="col-4 col-form-label">Phone<span class="text-danger">*</span></label>
            <input type="number" required name="user_phone" class="form-control" placeholder="Enter User Phone" value="<?php echo $userInfo[0]['user_phone']; ?>">
          </div>

          <div class="form-group">
            <label for="user_password" class="col-4 col-form-label">Password<span class="text-danger">*</span></label>
            <input type="password" minlength="6" name="user_password" class="form-control" placeholder="******">
          </div>
                               
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>    
      <script  type="text/javascript" src="custom-admin-javascript/user/edit_user.js"></script>