
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Role
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">User Role</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header">
           <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addRole"> Add Role</button>
          </div>

            <!-- /.box-header -->
            <div class="box-body"> 
              <table id="roleTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>                                
                    <th>SL</th>
                    <th>Role</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->


            <!-- add modal start -->

            <div class="modal fade" id="addRole">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Role</h4>
                  </div>
                  <div class="modal-body">
                     <form id="addUserRole"  role="form" action="admin/user-role" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                           <div class="form-group">
                            <label for="user_role_name" class="col-4 col-form-label">Role Name<span class="text-danger">*</span></label>
                            <input type="text" required name="user_role_name" class="form-control" placeholder="Enter Role Name">
                          </div>     
                          <hr>

                          <div class="form-group">
                            <label for="user_role_name" class="col-4 col-form-label"> Permission<span class="text-danger"></span></label>
                          </div> 

                           <div class="row">
                            <div class="col-md-3">
                              <input type="checkbox" id="add_sermon" name="add_sermon"> Add Sermon
                            </div>

                            <div class="col-md-3">
                              <input type="checkbox" id="view_sermon" name="view_sermon"> View Sermon
                            </div>

                            <div class="col-md-3">
                              <input type="checkbox" id="edit_sermon" name="edit_sermon"> Edit Sermon
                            </div>

                            <div class="col-md-3">
                              <input type="checkbox" id="delete_sermon" name="delete_sermon"> Delete Sermon
                            </div>

                          </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->


          <!-- edit modal start -->

            <div class="modal fade" id="editModal">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit User Role</h4>
                  </div>
                  <div class="modal-body">
                       <div id="edit_user_role_div">
                       </div>
                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->












  
