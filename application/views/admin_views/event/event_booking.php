

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Event Booking
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Event Booking</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
            <!-- /.box-header -->
            <div class="box-body">            

              <table id="event_booking_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                 <thead>
                    <tr>
                     <th>SL</th>
                     <th>Name</th>
                     <th>Seat Need?</th>
                     <th>Event Name</th>
                     <th>Status</th>
                     <th>Action</th>
                    </tr>
                 </thead>
                <tbody>
                </tbody>
              </table>

            </div>
            <!-- /.box-body -->


              <!-- edit modal start -->
            <div class="modal fade" id="exampleModal2">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">View Booking Info</h4>
                  </div>
                  <div class="modal-body">
                       <div id="view_booking_div">
                       </div>
                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->





          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->











  
