

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gallery Images
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Gallery Images</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header">
           <?php $permission = $this->permission->hasAccess(array('add_gallery_image','view_gallery_image','edit_gallery_image','delete_gallery_image')); ?>

           <?php if($permission['add_gallery_image']==1){ ?>
           <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_gallery"> Add Gallery Images </button>
           <?php } ?>
          </div>

            <?php if($permission['view_gallery_image']==1){ ?>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="galleryImage" class="table table-striped table-bordered" cellspacing="0" width="100%">
                 <thead>
                  <tr>
                    <th>SL</th>
                    <th>Image</th>
                    <th>Gallery</th>
                    <th>Description</th>
                    <th>Feature Image</th>
                    <th>Order</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            <?php } ?>


            <?php if($permission['add_gallery_image']==1){ ?>
            <!-- add modal start -->
            <div class="modal fade" id="add_gallery">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Gallery Image</h4>
                  </div>
                  <div class="modal-body">
                     <form id="addGallery"  role="form" action="admin/gallery-images" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                         <!--  <div class="form-group">
                             <label for="image_name" class="col-form-label">Image<span class="text-danger">*</span></label>
                           <input type="file" name="image_name" placeholder="Select a Image" accept="image/*">
                          </div>   -->                       

                          <div class="form-group">
                             <label for="image_name" class="col-form-label">Image<span class="text-danger">*</span></label>
                                    <div class="dropzone" >
                                            <div class="dz-message" >
                                                <h3> Click Here to select images</h3>
                                            </div>
                                    </div>
                          </div>

                           <div class="previews" id="preview"></div>

                            <div class="form-group">
                            <label for="gallery_category_id" class="col-4 col-form-label">Gallery<span class="text-danger">*</span></label>
                            <select required="required" name ="gallery_category_id" class="form-control select2" style="width: 100%;">
                                <option value="">---</option>
                                <?php foreach ($view_gallery_category as $category) { ?>
                                <option value="<?php echo $category['gallery_category_id'] ?>"><?php echo $category['gallery_category_name'] ?></option>
                                <?php } ?>
                              </select>
                          </div>

                          <div class="form-group">
                            <label for="is_featured_image" class="col-4 col-form-label">Feature Image<span class="text-danger">*</span></label>
                            <select required="required" name ="is_featured_image" class="form-control select2" style="width: 100%;">
                                <option value="0">NO</option>
                                <option value="1">YES</option>
                              </select>
                          </div>

                          <div class="form-group">
                            <label for="image_order" class="col-4 col-form-label">Order<span class="text-danger">*</span></label>
                            <input type="number" min="0" name="image_order" class="form-control"
                                                placeholder="Enter Order (Ex: 0)">
                          </div>



                          <div class="form-group">
                            <label for="image_description" class="col-4 col-form-label">Description<span class="text-danger">*</span></label>
                            <input type="text" name="image_description" class="form-control"
                                                placeholder="Enter Description">
                          </div>
                                               
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <?php } ?>

            <?php if($permission['edit_gallery_image']==1){ ?>
              <!-- edit modal start -->
            <div class="modal fade" id="edit_gallery_item">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Gallery Item</h4>
                  </div>
                  <div class="modal-body">
                       <div id="edit_gallery_div">
                       </div>
                  </div>                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <?php } ?>


          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
