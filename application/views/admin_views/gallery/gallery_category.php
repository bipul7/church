

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gallery
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Gallery</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header">
          <?php $permission = $this->permission->hasAccess(array('add_gallery','view_gallery','edit_gallery','delete_gallery')); ?>
           
           <?php if($permission['add_gallery']==1){ ?>
           <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_gallery_category"> Add Gallery</button>           
           <?php } ?>
           
           <?php if($permission['delete_gallery']==1){ ?>
           <span class="delete_all btn btn-danger"> Delete All </span>
           <?php } ?>

          </div>

            <?php if($permission['view_gallery']==1){ ?>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="galleryCategoryTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th>
                    <input type="checkbox" id="master">
                  </th>
                  <th>SL</th>
                  <th>Name</th>
                  <th>Action</th>
                </tr>
                </thead>

                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            <?php } ?>

            <?php if($permission['add_gallery']==1){ ?>
            <!-- add modal start -->
            <div class="modal fade" id="add_gallery_category">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Gallery</h4>
                  </div>
                  <div class="modal-body">
                     <form id="addGalleryCategory"  role="form" action="admin/gallery" method="POST" enctype="multipart/form-data">
                        <div class="box-body">

                          <div class="form-group">
                            <label for="gallery_category_name" class="col-4 col-form-label">Name<span class="text-danger">*</span></label>
                            <input type="text" name="gallery_category_name" class="form-control"
                                                placeholder="Enter Name">
                          </div>
                                               
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <?php } ?>

            <?php if($permission['edit_gallery']==1){ ?>
            <!-- edit modal start -->
            <div class="modal fade" id="edit_gallery_category">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Gallery</h4>
                  </div>
                  <div class="modal-body">
                       <form id="editGalleryCategory"  role="form" action="admin/update-gallery" method="POST" enctype="multipart/form-data">
                        <div class="box-body">

                          <div class="form-group">
                            <label for="gallery_category_name" class="col-4 col-form-label">Name<span class="text-danger">*</span></label>
                            <input type="text" name="gallery_category_name" id="e_gallery_category_name" class="form-control" placeholder="Enter Name">
                          </div>

                          <input type="hidden" name="gallery_category_key" id="e_gallery_category_key" class="form-control" placeholder="Enter Name">
                                               
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                      </form>
                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <?php } ?>
            
          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->

 










  
