<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.18/jquery.touchSwipe.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="assets/js/bootstrap-touch-slider-min.js"></script>
<script type="text/javascript" src="<?= $this->config->item('public_asset_path')?>/js/grid-gallery.min.js"></script>
<script type="text/javascript" src="<?= $this->config->item('public_asset_path')?>js/lightbox-plus-jquery.min.js"></script>
<script type="text/javascript" src="<?= $this->config->item('public_asset_path')?>js/slick.min.js"></script>
<script type="text/javascript" src="<?= $this->config->item('public_asset_path')?>js/timer.js"></script>
