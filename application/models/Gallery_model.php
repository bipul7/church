<?php

class Gallery_model extends CI_Model
{

    public function insert($table_name, $data)
    {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function getAllData($tableName)
    {
        $this->db->select('*');
        $query = $this->db->get($tableName)->result_array();
        return $query;
    }

    public function deleteGalleryImage($id)
    {
        $this->db->where('image_key', $id);
        return $query = $this->db->delete('gallery');
    }

    public function getItemInfo($id)
    {
        $this->db->select('*');
        $this->db->from('gallery');
        $this->db->where('image_key', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getGalleryImage()
    {
        $this->db->select('gallery.*, gallery_category.gallery_category_name');
        $this->db->from('gallery');
        $this->db->join('gallery_category', 'gallery_category.gallery_category_id=gallery.gallery_category_id', 'left');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function updateGalleryItem($data)
    {
        $image_key = $data['image_key'];
        $this->db->set('image_description', $data['image_description']);
        $this->db->set('is_featured_image', $data['is_featured_image']);
        $this->db->set('image_order', $data['image_order']);
        if (isset($data['image_name'])) {
            $this->db->set('image_name', $data['image_name']);
        }
        $this->db->set('gallery_category_id', $data['gallery_category_id']);
        $this->db->set('image_updated_at', $data['image_updated_at']);
        $this->db->where('image_key', $image_key);
        return $query = $this->db->update('gallery');
    }

    public function updateGalleryCategory($data)
    {
        $gallery_key = $data['gallery_category_key'];
        $this->db->set('gallery_category_name', $data['gallery_category_name']);
        $this->db->set('category_updated_at', $data['category_updated_at']);
        $this->db->where('gallery_category_key', $gallery_key);
        return $query = $this->db->update('gallery_category');
    }


    public function deleteGalleryCategory($id)
    {
        $this->db->where('gallery_category_key', $id);
        return $query = $this->db->delete('gallery_category');
    }

    public function deleteMassGalleryCategory($all_id)
    {
        $this->db->where_in('gallery_category_key', $all_id);
        return $query = $this->db->delete('gallery_category');
    }

    //Code for DataTable
    function get_datatables($table)
    {
        $this->_get_datatables_query($table);
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result_array();
    }

    private function _get_datatables_query($table)
    {
        if($table=='gallery_category'){
            $table = 'gallery_category';
            $column_order = array('gallery_category_name',null); 
            $column_search = array('gallery_category_name'); 
            $order = array('gallery_category_id' => 'desc');


            $this->db->from($table);
        $i = 0;
        foreach ($column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($order))
        {            
            $this->db->order_by(key($order), $order[key($order)]);
        }


        }

        if($table=='gallery'){
            $table = 'gallery';
            $column_order = array('image_id','gallery_category_name','image_order',null); 
            $column_search = array('gallery_category_name','image_description','image_order'); 
            $order = array('image_id' => 'desc');

            $this->db->select('gallery.*, gallery_category.gallery_category_name');
            $this->db->from('gallery');
            $this->db->join('gallery_category', 'gallery_category.gallery_category_id=gallery.gallery_category_id', 'left');
            
            $i = 0;
            foreach ($column_search as $item) // loop column
            {
                if($_POST['search']['value']) // if datatable send POST for search
                {
                    if($i===0) // first loop
                    {
                        $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
                    if(count($column_search) - 1 == $i) //last loop
                        $this->db->group_end(); //close bracket
                }
                $i++;
            }
            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }
            else if(isset($order))
            {
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }
        
    }

    public function count_all($table)
    {
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    function count_filtered($table)
    {
        $this->_get_datatables_query($table);
        $query = $this->db->get();
        return $query->num_rows();
    }


    //---------------------------------------------------------Front----------------------------------------------------

    public function getFeaturedImagesFromGallery($limit)
    {
        $this->db->select('*');
        $this->db->from('gallery');
        $this->db->where('image_name!=', null);
        $this->db->where('image_name!=', '');
        $this->db->where('is_featured_image', 1);
        $this->db->order_by('image_order', 'asc');
        $this->db->limit($limit);

        $gallery_items =  $this->db->get()->result_array();
        return $gallery_items;
    }

    public function getGalleryItems($category_key, $limit, $offset)
    {
        $this->db->select('*');
        $this->db->from('gallery as g');
        $this->db->join('gallery_category as gc', 'g.gallery_category_id=gc.gallery_category_id', 'left');
        $this->db->where('g.image_name!=', null);
        $this->db->where('g.image_name!=', '');

        if ($category_key != "") {
            $this->db->where('gc.gallery_category_key', $category_key);
        }

        $this->db->order_by('g.image_created_at', 'desc');
        $this->db->limit($limit, $offset);

        $gallery_items =  $this->db->get()->result_array();

        if (!empty($gallery_items)) {
            $gallery_items = array_map("self::put_related_items_in_gallery_items", $gallery_items);
        }

        return $gallery_items;
    }

    public function put_related_items_in_gallery_items($gallery_item)
    {
        if (!empty($gallery_item)) {
            $gallery_item['image_name_with_path'] = $this->config->item("default_image_placeholder");
            if (!empty($gallery_item['image_name'])) {
                $gallery_item['image_name_with_path'] =
                    $this->config->item("gallery_source_path") . $gallery_item['image_name'];
            }
        }

        return $gallery_item;
    }

    public function getGalleryCategories()
    {
        return $this->db->select('*')->from('gallery_category')->get()->result_array();
    }


}