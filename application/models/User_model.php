<?php
class User_model extends CI_Model {    

	public function insert($table_name,$data)
    {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

	public function getAllData($tableName)
    {
        $this->db->select('*');
        $query = $this->db->get($tableName)->result_array();
        return $query;
    }

    public function getAllUser(){
        $this->db->select('*');
        $this->db->from('user'); 
        $result = $this->db->get();
        return $result->result_array();
    }

     public function checkUser($email){
        $this->db->select('*');
        $this->db->from('user'); 
        $this->db->where('user_email', $email);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function checkUserusername($username){
        $this->db->select('*');
        $this->db->from('user'); 
        $this->db->where('user_username', $username);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function deleteUser($id)
    {
        $this->db->where('user_key', $id);
        return $query = $this->db->delete('user'); 
    }

    public function getUserInfo($id){
        $this->db->select('*');
        $this->db->from('user'); 
        $this->db->where('user_key', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_user_info($id){
        $this->db->select('*');
        $this->db->from('user'); 
        $this->db->where('user_id', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function updateUser($data)
    {   
    	$user_id = $data['user_id'];       
        $this->db->set('user_username', $data['user_username']);
        $this->db->set('user_email', $data['user_email']);
        $this->db->set('user_phone', $data['user_phone']);
        if(isset($data['user_password'])){
        	$this->db->set('user_password', $data['user_password']);
        }
        if(isset($data['user_image'])){
            $this->db->set('user_image', $data['user_image']);
        }
        $this->db->where('user_id', $user_id);
        return  $query=$this->db->update('user');
    }



    //////////////////// User Role /////////////////////////
    
    public function getAllRole(){
        $this->db->select('*');
        $this->db->from('user_role'); 
        $result = $this->db->get();
        return $result->result_array();
    }

    public function checkUserRole($roleName){
        $this->db->select('*');
        $this->db->from('user_role'); 
        $this->db->where('user_role_name', $roleName);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getUserRoleInfo($role_key){
        $this->db->select('*');
        $this->db->from('user_role'); 
        $this->db->where('user_role_key', $role_key);
        $result = $this->db->get();
        return $result->result_array();
    }
    
    public function getRoleName($role_id){
        $this->db->select('*');
        $this->db->from('user_role'); 
        $this->db->where('user_role_id', $role_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function check_role_name($role_name){
        $this->db->select('*');
        $this->db->from('user_role'); 
        $this->db->where('user_role_name', $role_name);
        $result = $this->db->get();
        return $result->result_array();
    }


     public function updateUserRole($data)
    {   
        $user_role_id = $data['user_role_id'];       
        $this->db->set('user_role_name', $data['user_role_name']);
        $this->db->set('user_role_permission', $data['user_role_permission']);
        $this->db->set('user_role_updated_at', $data['user_role_updated_at']);       
        $this->db->where('user_role_id', $user_role_id);
        return  $query=$this->db->update('user_role');
    }

    //Code for DataTable
    function get_datatables($table)
    {
        $this->_get_datatables_query($table);
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result_array();
    }

    private function _get_datatables_query($table)
    {
        if($table=='user_role'){
            $table = 'user_role';
            $column_order = array('user_role_name',null); 
            $column_search = array('user_role_name'); 
            $order = array('user_role_id' => 'desc');
        }

        if($table=='user'){
            $table = 'user';
            $column_order = array('user_id','user_username','user_email','user_phone',null); 
            $column_search = array('user_username','user_email','user_phone'); 
            $order = array('user_id' => 'desc');
        }
        $this->db->from($table);
        $i = 0;
        foreach ($column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if($order)
        {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function count_all($table)
    {
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    function count_filtered($table)
    {
        $this->_get_datatables_query($table);
        $query = $this->db->get();
        return $query->num_rows();
    }

    

    







    

    


    
     
	


	
}