<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends CI_Model
{

    var $table = 'settings';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function setSettings($settings_type, $settings_key, $settings_val)
    {
        $settings_row = $this->getSettings($settings_type, $settings_key, $only_value = false);//check if the entire row exists

        if (empty($settings_row)) {
            $ins_data = array();
            $ins_data['settings_type'] = $settings_type;
            $ins_data['settings_key'] = $settings_key;
            $ins_data['settings_val'] = $settings_val;

            $this->db->insert($this->table, $ins_data);
        }

        if (!empty($settings_row)) {
            $upd_data = array();
            $upd_data['settings_val'] = $settings_val;
            $this->db->update($this->table, $upd_data, array('settings_type' => $settings_type, 'settings_key' => $settings_key));
        }
    }

    public function getSettings($type, $key, $only_value)
    {
        $ret = null;

        $this->db->select('*');
        $this->db->from($this->table);

        $this->db->where('settings_type', $type);
        $this->db->where('settings_key', $key);

        $query = $this->db->get();

        $row = $query->row_array();

        if (!empty($row)) {
            $ret = $row['settings_val'];
        }

        return $only_value ? $ret : $row;

    }

    public function getSectionSettings($type)
    {
        $section_settings = array();
        $section_settings_list = $this->getSettingsList($type);

        if (!empty($section_settings_list)) {
            foreach ($section_settings_list as $section_settings_list_item) {
                if (!empty($section_settings_list_item['settings_key'])) {
                    $section_settings[$section_settings_list_item['settings_key']] = $section_settings_list_item['settings_val'];
                }
            }
        }

        return $section_settings;
    }

    public function getSettingsList($type)
    {
        $this->db->select('*');
        $this->db->from($this->table);

        $this->db->where('settings_type', $type);

        $query = $this->db->get();

        return $query->result_array();

    }


    public function get_stripe_test_mode()
    {
        $test_mode = 'on';
        $res = $this->getSettings('stripe', 'test_mode', $only_value = true);

        if (!empty($res)) {
            $test_mode = $res;
        }

        return $test_mode;
    }

    public function get_stripe_secret_key()
    {
        $test_mode = $this->get_stripe_test_mode();
        $key = $test_mode == 'on' ? 'tsk' : 'lsk';
        return $this->getSettings('stripe', $key, $only_value = true);

    }

    public function get_stripe_public_key()
    {
        $test_mode = $this->get_stripe_test_mode();
        $key = $test_mode == 'on' ? 'tpk' : 'lpk';
        return $this->getSettings('stripe', $key, $only_value = true);

    }

    //------------------------------------------------------------------------------------------------------------------

    public function site_name()
    {
        return $this->getSettings('main', 'site_name', $only_value = true);
    }

    public function site_logo()
    {
        $site_logo = "";

        $logo = $this->getSettings('main', 'site_logo', $only_value = true);

        if (empty($logo)) {
            $site_logo = $this->config->item('default_image_placeholder');
        } else {
            $site_logo = $this->config->item('settings_source_path') . $logo;
        }

        return $site_logo;
    }

    public function site_icon()
    {
        $site_icon = "";

        $icon = $this->getSettings('main', 'site_icon', $only_value = true);

        if (empty($icon)) {
            $site_icon = $this->config->item('default_image_placeholder');
        } else {
            $site_icon = $this->config->item('settings_source_path') . $icon;
        }

        return $site_icon;
    }

    public function footer_text()
    {
        return $this->getSettings('main', 'footer_text', $only_value = true);
    }

    //------------------------------------------------------------------------------------------------------------------

    public function all_prepared_settings()
    {
        $all_prepared_settings = array();

        $all_prepared_settings['main'] = $this->prepared_main_settings();
        $all_prepared_settings['social'] = $this->prepared_social_settings();

        return $all_prepared_settings;

    }

    public function default_main_settings()
    {
        $prepared_main_settings = array();
        $prepared_main_settings['site_name'] = "";
        $prepared_main_settings['site_description'] = "";
        $prepared_main_settings['address'] = "";
        $prepared_main_settings['email'] = "";
        $prepared_main_settings['phone'] = "";

        $prepared_main_settings['footer_text'] = "";
        $prepared_main_settings['site_logo'] = $this->config->item('default_image_placeholder');
        $prepared_main_settings['site_icon'] = $this->config->item('default_image_placeholder');

        return $prepared_main_settings;
    }

    public function prepared_main_settings()
    {
        $original_main_settings = $this->getSectionSettings($settings_type = "main");

        $prepared_main_settings = $this->default_main_settings();

        if (!empty($original_main_settings)) {

            foreach ($original_main_settings as $original_main_setting_k => $original_main_setting_v) {
                $prepared_main_settings[$original_main_setting_k] = $original_main_setting_v;

                if (in_array($original_main_setting_k, array('site_logo', 'site_icon'))) {
                    $prepared_main_settings[$original_main_setting_k] = $this->config->item('settings_source_path') . $original_main_setting_v;
                }
            }

        }

        return $prepared_main_settings;
    }

    public function default_social_settings()
    {
        $prepared_social_settings = array();
        $prepared_social_settings['facebook_link'] = "";
        $prepared_social_settings['twitter_link'] = "";
        $prepared_social_settings['google_link'] = "";
        $prepared_social_settings['youtube_link'] = "";

        return $prepared_social_settings;
    }

    public function prepared_social_settings()
    {
        $original_social_settings = $this->getSectionSettings($settings_type = "social");

        $prepared_social_settings = $this->default_social_settings();

        if (!empty($original_social_settings)) {

            foreach ($original_social_settings as $original_social_setting_k => $original_social_setting_v) {
                $prepared_social_settings[$original_social_setting_k] = $original_social_setting_v;

                if (in_array($original_social_setting_k, array('site_logo', 'site_icon'))) {
                    $prepared_main_settings[$original_social_setting_k] = $this->config->item('settings_source_path') . $original_social_setting_v;
                }
            }

        }

        return $prepared_social_settings;
    }


}