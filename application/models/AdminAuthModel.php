<?php
class AdminAuthModel extends CI_Model {

	public function checkLogin($post)
	{    
		$this->db->select('staff_id as id, staff_key, email, staff_type_id as role, staff_name as name');
		$this->db->where('email', $post['email']);
		$this->db->where('password', $post['password']);
		$query = $this->db->get('staff');
		$userInfo = $query->row();		
		return $userInfo; 
	}

	public function emailCheck( $email )
	{
		$this->db->select("*");
		$this->db->where('email', $email);
		$qry = $this->db->get('staff');
		return ( count( $qry->result() )>0 ) ? 1:0;
	}

	public function infoByEmail($email)
	{
		$admin = $this->db->where('email', $email)->get('staff')->result_array();
		return $admin[0];
	}

	public function saveAuthCode($authCode, $email)
    {	       
		$this->db->where('email', $email);
		$this->db->update('staff', array('authcode'=>$authCode) );
    }

    public function getEmailTempltateById($id)
    {
    	$this->db->select('*');
    	$this->db->from('email_template');
    	$this->db->where('email_template_id',$id);
    	$query = $this->db->get();
    	$row = $query->row();
    	return $row;
    }

    

}