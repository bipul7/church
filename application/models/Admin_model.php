<?php
class Admin_model extends CI_Model {

	public function insert($table_name,$data)
    {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

	public function getAllData($tableName)
    {
        $this->db->select('*');
        $query = $this->db->get($tableName)->result_array();
        return $query;
    }
    
     public function checkAdmin($email){
        $this->db->select('*');
        $this->db->from('admin'); 
        $this->db->where('email', $email);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function checkUsername($username){
        $this->db->select('*');
        $this->db->from('admin'); 
        $this->db->where('username', $username);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getAllAdmin(){
        $this->db->select('*');
        $this->db->from('admin'); 
        $this->db->where('role!=', 0);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getAdminInfo($id){
        $this->db->select('*');
        $this->db->from('admin'); 
        $this->db->where('id', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function updateAdmin($data)
    {   
    	$id = $data['id'];       
        $this->db->set('username', $data['username']);
        $this->db->set('email', $data['email']);
        if(isset($data['password'])){
        	$this->db->set('password', $data['password']);
        }
        if(isset($data['image'])){
            $this->db->set('image', $data['image']);
        }
        $this->db->where('id', $id);
        return  $query=$this->db->update('admin');
    }

    public function deleteAdmin($id)
    {
        $this->db->where('admin_key', $id);
        return $query = $this->db->delete('admin'); 
    }

    public function adminInfo($id){
        $this->db->select('*');
        $this->db->from('admin'); 
        $this->db->where('admin_key', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_admin_info($id){
        $this->db->select('id, admin_key, email, username, name, image, role, status');
        $this->db->from('admin'); 
        $this->db->where('id', $id);
        $result = $this->db->get();
        return $result->result_array();
    }


    public function updateAdminProfile($data)
    {   
        $admin_key = $data['admin_key'];       
        $this->db->set('username', $data['username']);
        if(isset($data['password'])){
            $this->db->set('password', $data['password']);
        }
        if(isset($data['image'])){
            $this->db->set('image', $data['image']);
        }
        $this->db->where('admin_key', $admin_key);
        return  $query=$this->db->update('admin');
    }


    
     
	


	
}