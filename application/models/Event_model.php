<?php

class Event_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    //---------------------------------------------------------Front----------------------------------------------------

    public function getActiveEventList($limit, $starts)
    {
        $query = $this->__eventListQuery($is_event_active = 1, $limit, $starts);

        $events = $query->result_array();

        if (!empty($events)) {
            $events = array_map("self::put_related_items_in_events", $events);
        }

        return $events;


    }

    public function countActiveEventList()
    {
        $query = $this->__eventListQuery($is_event_active = 1, $limit = "ignore", $starts = "ignore");

        return $query->num_rows();
    }

    private function __eventListQuery($is_event_active, $limit, $starts)
    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('is_event_active', $is_event_active);

        $this->db->order_by('event_starts', 'asc');


        if ($limit != "ignore" && $starts != "ignore") {
            $this->db->limit($limit, $starts);
        } else if ($limit != "ignore" && $starts == "ignore") {
            $this->db->limit($limit);
        }


        $query = $this->db->get();
        return $query;
    }

    public function getEvent($event_id)
    {
        $event = $this->__getEvent($event_id);

        if (!empty($event)) {
            $event = $this->put_related_items_in_events($event);
        }

        return $event;

    }

    public function getEventByKey($event_key)
    {
        $event = $this->__getEventByKey($event_key);

        if (!empty($event)) {
            $event = $this->put_related_items_in_events($event);
        }

        return $event;

    }

    private function __getEvent($event_id)
    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('event_id', $event_id);

        return $this->db->get()->row_array();
    }

    private function __getEventByKey($event_key)
    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('event_key', $event_key);

        return $this->db->get()->row_array();
    }

    public function getActiveFeaturedEvent($upcoming,$only_if_featured)
    {
        $event = $this->__getActiveFeaturedEvent($upcoming,$only_if_featured);

        if (!empty($event)) {
            $event = $this->put_related_items_in_events($event);
        }

        return $event;

    }

    private function __getActiveFeaturedEvent($upcoming,$only_if_featured)
    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('is_event_active', 1);

        if($only_if_featured){
            $this->db->where('is_event_featured', 1);
        }


        $this->db->where('event_starts!=', null);
        $this->db->where('event_starts!=', "");

        if($upcoming){
            $this->db->where('event_starts >=', date('Y:m:d H:i:s'));
        }

        $this->db->order_by('event_starts', 'asc');
        $this->db->limit(1);

        return $this->db->get()->row_array();
    }

    public function getActiveEvents($limit)
    {
        $events = $this->__getActiveEvents($limit);

        if (!empty($events)) {
            $events = array_map("self::put_related_items_in_events", $events);
        }

        return $events;

    }


    public function put_related_items_in_events($event)
    {
        if (!empty($event)) {
            $event['event_images'] = $this->getEventImages($event['event_id']);
            $event['featured_image'] = $this->getEventFeaturedImage($event['event_id']);

            $event['prev_event'] = $this->_getPrevEvent($event['event_id'], $event['event_starts'], $is_event_active = 1);
            $event['next_event'] = $this->_getNextEvent($event['event_id'], $event['event_starts'], $is_event_active = 1);
        }

        return $event;
    }

    private function _getPrevEvent($event_id, $event_starts, $is_event_active)
    {
        $ret = array();
        if (!empty($event_starts)) {

            $this->db->select('*');
            $this->db->from('event');
            $this->db->where('event_id!=', $event_id);

            $this->db->where('event_starts <=', $event_starts);
            $this->db->where('is_event_active', $is_event_active);

            $ret = $this->db->get()->row_array();

        }

        return $ret;

    }

    private function _getNextEvent($event_id, $event_starts, $is_event_active)
    {
        $ret = array();
        if (!empty($event_starts)) {

            $this->db->select('*');
            $this->db->from('event');
            $this->db->where('event_id!=', $event_id);
            $this->db->where('event_starts >=', $event_starts);
            $this->db->where('is_event_active', $is_event_active);

            $ret = $this->db->get()->row_array();

        }

        return $ret;
    }

    private function __getActiveEvents($limit)
    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('is_event_active', 1);
        $this->db->order_by('event_starts', 'asc');
        $this->db->limit($limit);

        return $this->db->get()->result_array();
    }

    public function getEventImages($event_id)
    {
        $this->db->select('*');
        $this->db->from('event_image');
        $this->db->where('event_id', $event_id);

        $this->db->where('event_image_name!=', null);
        $this->db->where('event_image_name!=', '');

        $event_images = $this->db->get()->result_array();

        $event_images = $this->put_related_items_in_event_images($event_images);

        return $event_images;


    }

    public function put_related_items_in_event_images($event_image)
    {
        $event_image_name = "";
        $event_image_name_with_path = $this->config->item("default_image_placeholder");
        if (!empty($event_image['event_image_name'])) {
            $event_image_name = $event_image['event_image_name'];
            $event_image_name_with_path = $this->config->item("event_source_path") . $event_image['event_image_name'];
        }

        $event_image['event_image_name'] =  $event_image_name;
        $event_image['event_image_name_with_path'] = $event_image_name_with_path;

        return $event_image;
    }

    public function getEventFeaturedImage($event_id)
    {
        $this->db->select('*');
        $this->db->from('event_image');
        $this->db->where('event_id', $event_id);

        $this->db->where('event_image_name!=', null);
        $this->db->where('event_image_name!=', '');
        $this->db->where('is_event_image_featured', 1);
        $this->db->limit(1);

        $event_image = $this->db->get()->row_array();

        $event_image['event_image_name_with_path'] = $this->config->item("default_image_placeholder");
        if (!empty($event_image)) {

            if (!empty($event_image['event_image_name'])) {
                $event_image['event_image_name_with_path'] = $this->config->item("event_source_path") . $event_image['event_image_name'];
            }
        } else {
            $event_image['event_image_name'] = "";
        }

        return $event_image;
    }


    //---------------------------------------------------------Backend--------------------------------------------------

    public function insert($table_name, $data)
    {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function getAllData($tableName)
    {
        $this->db->select('*');
        $query = $this->db->get($tableName)->result_array();
        return $query;
    }

    public function viewEventFeatureImage($event_id)
    {
        $this->db->select('*');
        $this->db->from('event_image');
        $this->db->where('event_id', $event_id);
        $this->db->where('is_event_image_featured', 1);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_event_info($event_id)
    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('event_key', $event_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getEventImage($event_id)
    {
        $this->db->select('*');
        $this->db->from('event_image');
        $this->db->where('event_id', $event_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function deletePrevEventImage($id)
    {
        $this->db->where('event_id', $id);
        return $query = $this->db->delete('event_image');
    }


    public function updateEvent($data)
    {
        $event_id = $data['event_id'];
        $this->db->set('event_name', $data['event_name']);
        $this->db->set('event_description', $data['event_description']);
        $this->db->set('event_location', $data['event_location']);
        $this->db->set('event_starts', $data['event_starts']);
        if (isset($data['event_ends'])) {
            $this->db->set('event_ends', $data['event_ends']);
        }
        $this->db->set('event_updated_at', $data['event_updated_at']);
        $this->db->set('is_event_active', $data['is_event_active']);
        $this->db->set('is_event_featured', $data['is_event_featured']);
        $this->db->set('event_order', $data['event_order']);
        $this->db->set('event_phone', $data['event_phone']);
        $this->db->set('event_website', $data['event_website']);
        $this->db->set('event_email', $data['event_email']);
        $this->db->set('event_seat', $data['event_seat']);
        $this->db->set('is_event_booking_enabled', $data['is_event_booking_enabled']);
        $this->db->set('event_google_map_latitude', $data['event_google_map_latitude']);
        $this->db->set('event_google_map_longitude', $data['event_google_map_longitude']);
        $this->db->set('event_google_map_location', $data['event_google_map_location']);
        $this->db->where('event_id', $event_id);
        return $query = $this->db->update('event');
    }


    public function deleteEventImage($id)
    {
        $this->db->where('event_image_id', $id);
        return $query = $this->db->delete('event_image');
    }

    public function getEventInfo($event_key)
    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('event_key', $event_key);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function eventImage($event_id)
    {
        $this->db->select('*');
        $this->db->from('event_image');
        $this->db->where('event_id', $event_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function deleteEvent($event_key)
    {
        $this->db->where('event_key', $event_key);
        return $query = $this->db->delete('event');
    }

     public function getEventBooking(){
        $this->db->select('event_booking.*, event.event_name');
        $this->db->from('event_booking');
        $this->db->join('event', 'event_booking.event_id = event.event_id', 'left');  
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_booking_info($booking_key){
        $this->db->select('event_booking.*, event.event_name');
        $this->db->from('event_booking'); 
        $this->db->join('event', 'event_booking.event_id = event.event_id', 'left');  
        $this->db->where('event_booking_key', $booking_key);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_eventInfo($event_id){
        $this->db->select('*');
        $this->db->from('event'); 
        $this->db->where('event_id', $event_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function total_seat_booking($event_id){
        $this->db->select('sum(event_booking.event_booking_seat) as total_booking');
        $this->db->from('event_booking'); 
        $this->db->where('event_id', $event_id);
        $this->db->where('event_booking_status', 1);
        $result = $this->db->get();
        $ret = $result->result_array();
        return $ret[0]['total_booking'];
    }

    public function approve_booking($booking_key){
        $this->db->set('event_booking_status', 1);
        $this->db->where('event_booking_key', $booking_key);
        return  $query=$this->db->update('event_booking');
    }

    public function unapprove_booking($booking_key){
        $this->db->set('event_booking_status', 0);
        $this->db->where('event_booking_key', $booking_key);
        return  $query=$this->db->update('event_booking');
    }

    public function deleteEventBooking($booking_key)
    {
        $this->db->where('event_booking_key', $booking_key);
        return $query = $this->db->delete('event_booking'); 
    }

    
    public function get_booking_for_event($event_id){
        $this->db->select('event_booking.*, event.event_name');
        $this->db->from('event_booking'); 
        $this->db->join('event', 'event_booking.event_id = event.event_id', 'left');  
        $this->db->where('event_booking.event_id', $event_id);
        $this->db->where('event_booking.event_booking_status', 1);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function deleteMassEvent($all_id)
    {
        $this->db->where_in('event_key', $all_id);
        return $query = $this->db->delete('event'); 
    }


    //Code for DataTable
    function get_datatables($table)
    {
        $this->_get_datatables_query($table);
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result_array();
    }

    private function _get_datatables_query($table)
    {
        if($table=='event'){
            $table = 'event';
            $column_order = array('event_name','event_seat',null); 
            $column_search = array('event_name','event_seat'); 
            $order = array('event_id' => 'desc');

            $this->db->from($table);
        }

        if($table=='event_booking'){
            $table = 'event_booking';
            $column_order = array('event_name','event_booking_user_name','event_booking_seat',null); 
            $column_search = array('event_name','event_booking_user_name','event_booking_seat'); 
            $order = array('event_booking_id' => 'desc');
            $this->db->select('event_booking.*, event.event_name');
            $this->db->from('event_booking');
            $this->db->join('event', 'event_booking.event_id = event.event_id', 'left'); 
        }


        $i = 0;
        foreach ($column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($order))
        {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function count_all($table)
    {
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    function count_filtered($table)
    {
        $this->_get_datatables_query($table);
        $query = $this->db->get();
        return $query->num_rows();
    }


}