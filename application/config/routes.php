<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'HomeController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// auth
$route['login'] 	  = 'AuthController/login';
$route['register'] 	  = 'AuthController/register';
$route['check-login'] = 'AuthController/check_login';
$route['logout'] 	  = 'AuthController/logout';


//staff
$route['staffs']		 = 'StaffController/getStaffs';
$route['staff/(:any)']   = 'StaffController/getStaff/$1';

//event
$route['events'] 				= 'EventController/getEvents';
$route['event/(:any)']  		= 'EventController/getEvent/$1';
$route['ajax-event-list-items'] = 'EventController/ajax_event_list_items';

//gallery
$route['gallery'] 				= 'GalleryController';
$route['ajax-gallery-items'] 	= 'GalleryController/ajax_gallery_items';

//Blog
$route['blogs']					= 'BlogController/getBlogs';
$route['blog/(:any)'] 			= 'BlogController/getBlog/$1';
$route['ajax-blog-list-items']  = 'BlogController/ajax_blog_list_items';

//Sermon
$route['sermons'] 				 = 'SermonController/getSermons';
$route['sermon/(:any)'] 		 = 'SermonController/getSermon/$1';
$route['ajax-sermon-list-items'] = 'SermonController/ajax_sermon_list_items';
$route['render-sermon-file']	 = 'SermonController/render_sermon_file';



//---------------------------------API----------------------------------------------------------------------------------
$api_route = API_ROUTE;
$route[$api_route] = 'ApiController/hit';


//$route[$api_route.'test'] = 'ApiController/test';
//$route[$api_route.'get-active-banners'] = 'ApiController/getActiveBanners';

//keep this at the bottom
$route[$api_route.'/(:any)'] = 'ApiController/invalid_url';

//---------------------------------------------------Admin Panel--------------------------------------------------------
$route['admin'] 		                = 'AdminAuthController/login';
$route['admin/login'] 					= 'AdminAuthController/login';
$route['admin/check-login'] 			= 'AdminAuthController/check_login';

$route['admin/dashboard'] 				= 'AdminDashboardController/index';
$route['admin/logout'] 					= 'AdminAuthController/logout';

$route['admin/404_override'] 			= '';
$route['admin/translate_uri_dashes'] 	= FALSE;

//forgot password		
$route['admin/sendResetPassEmail'] 		= 'AdminAuthController/sendResetPassEmail';
$route['admin/passwordresetform'] 		= 'AdminAuthController/passwordResetForm';
$route['admin/updateresetpassword'] 	= 'AdminAuthController/updatePassword';
$route['admin/forgot_password'] 		= 'AdminAuthController/forgot_password';


///admin
$route['admin/all-admin'] 				    = 'AdminController/allAdmin';
$route['admin/update-admin'] 				= 'AdminController/updateAdmin';
$route['admin/render-edit-admin'] 			= 'AdminController/renderEditAdmin';
$route['admin/delete-admin'] 				= 'AdminController/deleteAdmin';
$route['admin/admin-profile'] 				= 'AdminController/adminProfile';

//settings
$route['admin/main-settings'] 				= 'AdminMainSettingsController';
$route['admin/page-settings'] 				= 'AdminPageSettingsController';
$route['admin/payment-settings'] 			= 'AdminPaymentSettingsController';
$route['admin/social-settings'] 			= 'AdminSocialSettingsController';

//gallery
$route['admin/gallery']						= 'AdminGalleryController/viewGalleryCategory';
$route['admin/update-gallery']				= 'AdminGalleryController/updateGalleryCategory';
$route['admin/delete-gallery']				= 'AdminGalleryController/deleteGalleryCategory';
$route['admin/gallery-images']				= 'AdminGalleryController/viewGallery';
$route['admin/render_edit_gallery']			= 'AdminGalleryController/renderEditGallery';
$route['admin/delete-gallery-image']		= 'AdminGalleryController/deleteGalleryImage';
$route['admin/update-gallery-item']			= 'AdminGalleryController/updateGalleryItem';
$route['admin/mass-gallery-category-delete']= 'AdminGalleryController/massGalleryCategoryDelete';
$route['admin/load-gallery-category-table']	= 'AdminGalleryController/loadGalleryCategoryTable';
$route['admin/load-gallery-image-table']	= 'AdminGalleryController/loadGalleryImageTable';


//Banner
$route['admin/slideshow']					= 'AdminBannerController/viewBanner';
$route['admin/delete-banner']				= 'AdminBannerController/deleteBanner';
$route['admin/render-edit-banner']			= 'AdminBannerController/renderEditBanner';
$route['admin/update-banner']				= 'AdminBannerController/updateBanner';
$route['admin/load-banner-table']			= 'AdminBannerController/loadBannerTable';


//Staff
$route['admin/staff']						= 'AdminStaffController/viewStaff';
$route['admin/render-edit-staff']			= 'AdminStaffController/renderEditStaff';
$route['admin/update-staff']				= 'AdminStaffController/updateStaff';
$route['admin/delete-staff-image']			= 'AdminStaffController/deleteStaffImage';
$route['admin/delete-staff']				= 'AdminStaffController/deleteStaff';
$route['admin/load-staff-table']			= 'AdminStaffController/loadStaffTable';
//$route['admin/mass-staff-type-delete']		= 'AdminStaffController/massStaffTypeDelete';

//staff Type
$route['admin/staff-type']				= 'AdminStaffController/staffType';
$route['admin/add-staff-type']			= 'AdminStaffController/addStaffType';
$route['admin/edit-staff-type/(:any)']	= 'AdminStaffController/editStaffType/$1';
$route['admin/update-staff-type']		= 'AdminStaffController/updateStaffType';
$route['admin/delete-staff-type']		= 'AdminStaffController/deleteStaffType';
$route['admin/profile']					= 'AdminStaffController/profile';
$route['admin/load-staff-type-table']	= 'AdminStaffController/loadStaffTypeTable';






//Testimonial
$route['admin/testimonial']					= 'AdminTestimonialController/viewTestimonial';
$route['admin/delete-testimonial']			= 'AdminTestimonialController/deleteTestimonial';
$route['admin/render-edit-testimonial']		= 'AdminTestimonialController/renderEditTestimonial';
$route['admin/update-testimonial']			= 'AdminTestimonialController/updateTestimonial';
$route['admin/mass-testimonial-delete']		= 'AdminTestimonialController/massTestimonialDelete';
$route['admin/load-testimonial-table']		= 'AdminTestimonialController/loadTestimonialTable';


//Event
$route['admin/event']					= 'AdminEventController/viewEvent';
$route['admin/render-edit-event']		= 'AdminEventController/renderEditEvent';
$route['admin/update-event']			= 'AdminEventController/updateEvent';
$route['admin/delete-event-image']		= 'AdminEventController/deleteEventImage';
$route['admin/delete-event']			= 'AdminEventController/deleteEvent';
$route['admin/view-event-booking/(:any)']		= 'AdminEventController/viewBookingForEvent/$1';
$route['admin/mass-event-delete']		= 'AdminEventController/massEventDelete';
$route['admin/load-event-table']	    = 'AdminEventController/loadEventTable';



//User
$route['admin/user']					= 'AdminUserController/viewUser';
$route['admin/delete-user']				= 'AdminUserController/deleteUser';
$route['admin/render-edit-user']		= 'AdminUserController/renderEditUser';
$route['admin/update-user']			    = 'AdminUserController/updateUser';
// UserRole
$route['admin/user-role']				= 'AdminUserController/viewUserRole';
$route['admin/render-edit-user-role']	= 'AdminUserController/renderEditUserRole';
$route['admin/update-user-role']		= 'AdminUserController/updateUserRole';
$route['admin/load-role-table']			= 'AdminUserController/loadRoleTable';
$route['admin/load-user-table']			= 'AdminUserController/loadUserTable';



// Event Booking
$route['admin/event-booking']			= 'AdminEventController/viewEventBooking';
$route['admin/render_view_booking']		= 'AdminEventController/renderViewBooking';
$route['admin/update-booking-status']	= 'AdminEventController/updateBookingStatus';
$route['admin/delete-event-booking']	= 'AdminEventController/deleteEventBooking';
$route['admin/load-event-booking-table']= 'AdminEventController/loadEventBookingTable';

// Sermon
$route['admin/sermon']					= 'AdminSermonController/viewSermon';
$route['admin/render_edit_sermon']		= 'AdminSermonController/renderEditSermon';
$route['admin/update-sermon']			= 'AdminSermonController/updateSermon';
$route['admin/delete-sermon']			= 'AdminSermonController/deleteSermon';


// Blog
$route['admin/blog']						= 'AdminBlogController/viewBlog';
$route['admin/load-blog-table']				= 'AdminBlogController/loadBlogTable';
$route['admin/render_edit_blog']			= 'AdminBlogController/renderEditBlog';
$route['admin/update-blog']					= 'AdminBlogController/updateBlog';
$route['admin/delete-blog']					= 'AdminBlogController/deleteBlog';
$route['admin/mass-blog-delete']			= 'AdminBlogController/massBlogDelete';
$route['admin/blog-comment/(:any)']			= 'AdminBlogController/blogComment/$1';
$route['admin/render_edit_blog_comment']	= 'AdminBlogController/renderEditBlogComment';
$route['admin/update-blog-comment-status']	= 'AdminBlogController/updateBlogCommentStatus';
$route['admin/delete-blog-comment']			= 'AdminBlogController/deleteBlogComment';

// Page
$route['admin/page']						= 'AdminPageController/viewPage';
$route['admin/render_edit_page']			= 'AdminPageController/renderEditPage';
$route['admin/delete-page']					= 'AdminPageController/deletePage';
$route['admin/update-page']					= 'AdminPageController/updatePage';




// fontend pages (must need to set this pages route at the bottom of all route)
$route['(:any)'] 				 = 'PageController/pages/$1';


