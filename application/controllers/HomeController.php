<?php
defined('BASEPATH') or exit('No direct script access allowed');

class HomeController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model("Banner_model");
        $this->load->model("Gallery_model");
        $this->load->model("Blog_model");
        $this->load->model("Staff_model");
        $this->load->model("Event_model");
        $this->load->model("Testimonial_model");
    }

    public function index()
    {
        $data = array();

        $data['meta'] = $this->template->render('segments/meta', array(), true);
        $data['headlink'] = $this->template->render('segments/headlink', array(), true);
        $data['navbar'] = $this->template->render('segments/navbar', array(), true);
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->render('segments/footlink', array(), true);
        $data['content'] = $this->home_content($content_params = array());

        $view = $this->load->view('master', $data, true);
        echo $view;
        exit;
    }


    public function home_content($content_params)
    {
        $data = array();
        $data['banner'] = $this->banner();
        $data['home_event'] = $this->event();
        $data['welcome_text'] = $this->template->render('contents/home/welcome_text',$data, true);
        $data['latest_sermon'] = $this->template->render('contents/home/latest_sermon',$data, true);
        $data['product_latest'] = $this->template->render('contents/home/product_latest',$data, true);
        $data['gallery'] = $this->gallery();
        $data['testmonial'] = $this->testimonial();
        $data['our_staff'] = $this->our_staff();
        //$data['latest_blog'] = $this->template->render('contents/home/latest_blog', $data, true);
        $data['latest_blog'] = $this->latest_blog();

        $content =  $this->template->render('contents/home/home_page',$data, true);

        return $content;
    }

    public function banner()
    {
        $data = array();

        $data['banners'] = $this->Banner_model->getActiveBanners($limit = 5);

        //echo "<pre>";print_r($data['banners']);exit;

        $banner =  $this->template->render('contents/home/banner',$data, true);

        return $banner;
    }

    public function event()
    {
        $data = array();

        $data['event'] = $this->Event_model->getActiveFeaturedEvent($upcoming = true,$only_if_featured = false);

        //echo "<pre>";print_r($data);exit;

        $event = $this->template->render('contents/home/home_event', $data, true);

        return $event;
    }

    public function our_staff()
    {
        $data = array();

        $data['staffs'] = $this->Staff_model->getActiveStaffs($limit = 150);

        //echo "<pre>";print_r($data);exit;

        $our_staff=  $this->template->render('contents/home/our_staff',$data, true);

        return $our_staff;
    }

    public function latest_blog()
    {
        $data = array();

        $data['latest_blog'] = $this->Blog_model->getRecentPosts($limit = 1);

        //echo "<pre>";print_r($data);exit;

        $latest_blog=  $this->template->render('contents/home/latest_blog', $data, true);

        return $latest_blog;
    }

    public function gallery()
    {
        $data = array();

        $data['featured_gallery_images'] = $this->Gallery_model->getFeaturedImagesFromGallery($limit = 5);

        $gallery =  $this->template->render('contents/home/gallery',$data, true);

        return $gallery;
    }

    public function testimonial()
    {
        $data = array();

        $data['testimonials'] = $this->Testimonial_model->getTestimonials($limit = 100);
        $testimonial = $this->template->render('contents/home/testmonial', $data, true);

        return $testimonial;
    }
}
