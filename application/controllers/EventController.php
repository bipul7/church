<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EventController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model("Event_model");
    }

    public function getEvent($event_key)
    {
        $data = array();

        $data['meta'] = $this->template->render('segments/meta', array(), true);
        $data['headlink'] = $this->template->render('segments/headlink', array(), true);
        $data['navbar'] = $this->template->render('segments/navbar', array(), true);
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->render('segments/footlink', array(), true);

        $content_params = array();
        $content_params['event_key'] = $event_key;
        $data['content'] = $this->event_content($content_params);

        $view = $this->load->view('master', $data, true);
        echo $view;
        exit;
    }


    public function event_content($content_params)
    {
        $data = array();

        $data['event'] = $this->Event_model->getEventByKey($content_params['event_key']);
        $data['google_map_api_key'] = $this->config->item('google_map_api_key');
        $data['event_google_map_zoom_level'] = "18";

        if(!empty($data['event'])){
            $data['title'] = $data['event']["event_name"];
        }

        $content = $this->template->render('contents/single_event/single_event_page', $data, true);

        return $content;
    }

    public function getEvents()
    {
        $data = array();

        $data['title'] = "Upcoming Events";

        $data['meta'] = $this->template->render('segments/meta', array(), true);
        $data['headlink'] = $this->template->render('segments/headlink', array(), true);
        $data['navbar'] = $this->template->render('segments/navbar', array(), true);
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->render('segments/footlink', array(), true);

        $data['content'] = $this->event_list_content(array());

        $view = $this->load->view('master', $data, true);
        echo $view;
        exit;
    }

    public function event_list_content($content_params)
    {
        $data = array();

        $data['events'] = array();

        $content = $this->template->render('contents/event_list/event_list_page', $data, true);

        return $content;
    }

    public function ajax_event_list_items()
    {
        $data = array();

        $page_input = $this->input->get("page");
        if (empty($page_input)) {
            $page_input = 1;

        }

        $limit = 2;
        $page = $page_input;

        $starts = ($page - 1) * $limit;

        $data['events'] = $this->Event_model->getActiveEventList($limit, $starts);

        if (empty($data['events'])) {
            echo " ";exit;
        }

        $content = $this->template->render('contents/event_list/event_list_items', $data, true);

        echo $content;
        exit;
    }


}