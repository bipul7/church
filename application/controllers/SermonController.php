<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include APPPATH."views\contents\blog\\".'htmlfixer.class.php';
class SermonController extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->model("Sermon_model");
    } 


    public function getSermon($sermon_key)
    {   
        $data = array();

        $data['meta'] = $this->template->render('segments/meta', array(), true);
        $data['headlink'] = $this->template->render('segments/headlink', array(), true);
        $data['navbar'] = $this->template->render('segments/navbar', array(), true);
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->render('segments/footlink', array(), true);

        $content_params = array();
        $content_params['sermon_key'] = $sermon_key;
        $data['content'] = $this->sermon_content($content_params);

        $view = $this->load->view('master', $data, true);
        echo $view;
        exit;
    }

    public function sermon_content($content_params)
    {
        $data = array();
        $data['sermon'] = $this->Sermon_model->getSermonByKey($content_params['sermon_key']);

        // echo "<pre>"; print_r($data['sermon']); die;
        $data['recent_posts'] = $this->Sermon_model->getRecentPosts($limit = 4);
        $data['recent_sermons'] = $this->Sermon_model->getRecentSermons($limit = 4);
        if(!empty($data['sermon'])){
            $data['title'] = $data['sermon']["sermon_title"];
        }
        $content = $this->template->render('contents/sermon/single_sermon_page', $data, true);
        return $content;
    }

     
    public function getSermons()
    {
        $data = array();

        $data['title'] = "Sermons";

        $data['meta']       = $this->template->render('segments/meta', array(), true);
        $data['headlink']   = $this->template->render('segments/headlink', array(), true);
        $data['navbar']     = $this->template->render('segments/navbar', array(), true);
        $data['footer']     = $this->template->footer(array());
        $data['footlink']   = $this->template->render('segments/footlink', array(), true);

        $data['content']    = $this->sermon_page_content($content_params = array());

        $view = $this->load->view('master', $data, true);
        echo $view;
        exit;
    }

    public function sermon_page_content($content_params)
    {
        $data = array();

       
        
        $data['sermon_list_content']   = $this->sermon_list_content($content_params);
        //$data['recent_post_content'] = $this->recent_post_content($content_params);
        //$data['tag_content']         = $this->tag_content($content_params);

        //$content = $this->template->render('contents/blog_list/blog_list_page', $data, true);

        $content =  $this->template->render('contents/sermon/sermon_list_page',$data, true);

        return $content;
    } 

    public function sermon_list_content($content_params)
    {
        $data = array(); 
        $content = $this->template->render('contents/sermon/sermon_list_content_page', $data, true);
        return $content;
    }

    public function ajax_sermon_list_items()
    {
        $data = array();

        $page_input = $this->input->get("page");
        if (empty($page_input)) {
            $page_input = 1;

        }
        $limit = 1;
        $page = $page_input;

        $starts = ($page - 1) * $limit;

        $data['sermons'] = $this->Sermon_model->getActiveSermonList($limit, $starts);

        if (empty($data['sermons'])) {
            echo " ";exit;
        }

        $content = $this->template->render('contents/sermon/sermon_list_items_page', $data, true);

        echo $content;
        exit;
    }


    public function render_sermon_file(){
        // echo "hit Render Sermon File";
        
        $sermon_file_key = $this->input->post('file_key');       
        $data['sermon_file_info']   =   $this->Sermon_model->get_sermon_file_info($sermon_file_key);

        $json = array();
        $json['sermon_file_info']            = $data['sermon_file_info'];
        $json['file_play_div'] = $this->load->view('contents/sermon/file_play_div', $data, TRUE);
        echo json_encode($json);
    }

    

    


}