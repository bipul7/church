<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include APPPATH."views\contents\blog\\".'htmlfixer.class.php';
class BlogController extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->model("Blog_model");
    }

    

    

    public function getBlog($blog_key)
    {   
        $data = array();

        $data['meta'] = $this->template->render('segments/meta', array(), true);
        $data['headlink'] = $this->template->render('segments/headlink', array(), true);
        $data['navbar'] = $this->template->render('segments/navbar', array(), true);
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->render('segments/footlink', array(), true);

        $content_params = array();
        $content_params['blog_key'] = $blog_key;
        $data['content'] = $this->blog_content($content_params);

        $view = $this->load->view('master', $data, true);
        echo $view;
        exit;
    }

    public function blog_content($content_params)
    {
        $data = array();
        $data['blog'] = $this->Blog_model->getBlogByKey($content_params['blog_key']);
        if(!empty($data['blog'])){
            $data['title'] = $data['blog']["blog_title"];
        }
        $content = $this->template->render('contents/single_blog/single_blog_page', $data, true);
        return $content;
    }

    

    public function getBlogs()
    {
        $data = array();

        $data['title'] = "Blogs";

        $data['meta']       = $this->template->render('segments/meta', array(), true);
        $data['headlink']   = $this->template->render('segments/headlink', array(), true);
        $data['navbar']     = $this->template->render('segments/navbar', array(), true);
        $data['footer']     = $this->template->footer(array());
        $data['footlink']   = $this->template->render('segments/footlink', array(), true);

        $data['content']    = $this->blog_page_content($content_params = array());

        $view = $this->load->view('master', $data, true);
        echo $view;
        exit;
    }

    public function blog_page_content($content_params)
    {
        $data = array();

       
        
        $data['blog_list_content']   = $this->blog_list_content($content_params);
        $data['recent_post_content'] = $this->recent_post_content($content_params);
        $data['tag_content']         = $this->tag_content($content_params);

        //$content = $this->template->render('contents/blog_list/blog_list_page', $data, true);

        $content =  $this->template->render('contents/blog/blog_list_page',$data, true);

        return $content;
    } 

    public function blog_list_content($content_params)
    {
        $data = array();

        $data['blogs'] = array();

        $content = $this->template->render('contents/blog/blog_list_content_page', $data, true);

        return $content;
    }

    public function ajax_blog_list_items()
    {
        $data = array();

        $page_input = $this->input->get("page");
        if (empty($page_input)) {
            $page_input = 1;

        }

        $limit = 2;
        $page = $page_input;

        $starts = ($page - 1) * $limit;

        $data['blogs'] = $this->Blog_model->getActiveBlogList($limit, $starts);

        if (empty($data['blogs'])) {
            echo " ";exit;
        }

        $content = $this->template->render('contents/blog/blog_list_items_page', $data, true);

        echo $content;
        exit;
    }

    public function recent_post_content($content_params)
    {
        $data = array();

        $data['recent_posts'] = $this->Blog_model->getRecentPosts($limit = 4);

        $recent_posts =  $this->template->render('contents/blog/recent_post_page',$data, true);

        return $recent_posts;
    }

    public function tag_content($content_params)
    {
        $data = array();

        $data['tags'] = $this->Blog_model->getTags($limit = 15);

        $tags =  $this->template->render('contents/blog/tags_page',$data, true);

        return $tags;
    }

    


}