<?php

class ApiController extends CI_Controller
{

    public $api_key;

    function __construct()
    {
        parent:: __construct();


        $this->api_key = $this->config->item('api_key');

        $this->load->model('Banner_model');



    }

    // privates <starts>------------------------------------------------------------------------------------------------
    private function __header()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        @header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    }

    private function __response_over_and_out($arr)
    {
        $this->__header();
        $json = json_encode($arr);
        echo $json;
        exit;

    }

    private function __rules($response_arr, $rules)
    {

        $error_count = 0;
        $messages = array();

        foreach ($rules as $index => $rule) {


            if (in_array('required', $rule)) {
                if (!$this->input->post($index)) {
                    $error_count++;
                    $messages[] = "{$index} is required";
                }
            }

            if (in_array('numeric', $rule)) {
                if (!is_numeric($this->input->post($index))) {
                    $error_count++;
                    $messages[] = "{$index} should be a number";

                }
            }

            if (isset($rule['max_range'])) {

                if (is_numeric($rule['max_range'])) {
                    if ($this->input->post($index) > $rule['max_range']) {
                        $error_count++;
                        $messages[] = "limit should be maximum {$rule['max_range']}";
                    }
                }

            }

            if (isset($rule['min_range'])) {

                if (is_numeric($rule['min_range'])) {
                    if ($this->input->post($index) < $rule['min_range']) {
                        $error_count++;
                        $messages[] = "limit should be minimum {$rule['min_range']}";
                    }
                }

            }

        }

        if ($error_count > 0) {

            $response_arr['error'] = true;
            $combined_messages = implode(" | ", $messages);
            $response_arr['message'] = $combined_messages;
            $this->__response_over_and_out($response_arr);
        }


    }

    private function __get_valid_actions()
    {
        //list all valid actions here
        $valid_actions = array();
        $valid_actions['test'] = "test";
        $valid_actions['get_active_banners'] = "getActiveBanners";

        return $valid_actions;
    }

    // privates <ends>--------------------------------------------------------------------------------------------------

    //utility <starts> ---------------------------------------------------------------------------------------------------

    public function invalid_url()
    {
        $response_arr['http_response_code'] = http_response_code();
        $response_arr['error'] = true;
        $response_arr['message'] = "Invalid url";
        $response_arr['data'] = array();

        $this->__response_over_and_out($response_arr);

    }


    public function check_api_key()
    {
        $response_arr = array();
        $posted_api_key = $this->input->post('api_key');

        if (!$posted_api_key) {
            $response_arr['http_response_code'] = http_response_code();
            $response_arr['error'] = true;
            $response_arr['message'] = "Need an API key";
            $response_arr['data'] = array();

            $this->__response_over_and_out($response_arr);
        }

        if ($posted_api_key != $this->api_key) {
            $response_arr['http_response_code'] = http_response_code();
            $response_arr['error'] = true;
            $response_arr['message'] = "API key does not matches";
            $response_arr['data'] = array();

            $this->__response_over_and_out($response_arr);
        }

        return true;
    }

    public function check_action()
    {
        $response_arr = array();

        $posted_action = $this->input->post('action');

        if (!$posted_action) {
            $response_arr['http_response_code'] = http_response_code();
            $response_arr['error'] = true;
            $response_arr['message'] = "Provide a specific action";
            $response_arr['data'] = array();

            $this->__response_over_and_out($response_arr);
        }

        //list all valid actions here
        $valid_actions = $this->__get_valid_actions();
        $valid_actions['test'] = "test";
        $valid_actions['get_active_banners'] = "getActiveBanners";

        if (!array_key_exists($posted_action, $valid_actions)) {
            $response_arr['http_response_code'] = http_response_code();
            $response_arr['error'] = true;
            $response_arr['message'] = "This action is invalid";
            $response_arr['data'] = array();

            $this->__response_over_and_out($response_arr);
        }

        return $valid_actions;


    }

    private function __email($post)
    {
        $this->load->library('email');

        $this->email->from('rsmai@rssoft.win', 'Mahmud RS');
        $this->email->to('mahmud@sahajjo.com');


        $this->email->subject('post Test');

        $this->email->message(print_r($post,true));

        $this->email->send();
    }


    //utility <ends> ---------------------------------------------------------------------------------------------------


    //this is the main function
    public function hit()
    {
        $this->__email($_REQUEST);

        $this->check_api_key();

        $valid_actions = $this->check_action();

        if (!empty($valid_actions) && is_array($valid_actions)) {
            //provided everything is ok

            $posted_action = $this->input->post('action');

            $method_name = $valid_actions[$posted_action];

            $this->{$method_name}();



        }

    }

    public function test()
    {
        $response_arr = array();

        try {
            $response_arr['http_response_code'] = http_response_code();
            $response_arr['error'] = false;
            $response_arr['message'] = "Api is connected";
            $response_arr['data'] = array();

        } catch (Exception $e) {
            $response_arr['http_response_code'] = http_response_code();
            $response_arr['error'] = true;
            $response_arr['message'] = $e->getMessage();
            $response_arr['data'] = array();

        }

        $this->__response_over_and_out($response_arr);

    }

    public function getActiveBanners()
    {
        $response_arr = array();

        try {
            $response_arr['http_response_code'] = http_response_code();
            $response_arr['error'] = false;
            $response_arr['message'] = "";
            $response_arr['data'] = array();
            $response_arr['data']['banners'] = array();

            $rules['limit'] = ['required', 'numeric', 'max_range' => 50, 'min_range' => 1];

            $this->__rules($response_arr, $rules);


            $limit = $this->input->post('limit');
            $banners = $this->Banner_model->getActiveBanners($limit);

            if (!empty($banners)) {
                $response_arr['data']['banners'] = $banners;
            }


        } catch (Exception $e) {
            $response_arr['http_response_code'] = http_response_code();
            $response_arr['error'] = true;
            $response_arr['message'] = $e->getMessage();


        }

        $this->__response_over_and_out($response_arr);
    }


}