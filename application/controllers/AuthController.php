<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AuthController extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model("AuthModel");
        $this->load->model('Important_model');
    }

    

    

    public function login()
    {   
        $data = array();

        $data['title'] = "Login";

        $data['meta']       = $this->template->render('segments/meta', array(), true);
        $data['headlink']   = $this->template->render('segments/headlink', array(), true);
        $data['navbar']     = $this->template->render('segments/navbar', array(), true);
        $data['footer']     = $this->template->footer(array());
        $data['footlink']   = $this->template->render('segments/footlink', array(), true);

        $data['content']    = $this->login_page_content($content_params = array());

        $view = $this->load->view('master', $data, true);
        echo $view;
        exit;
    }

    public function login_page_content($content_params)
    {
        $data = array(); 
        $content =  $this->template->render('contents/auth/login',$data, true);
        return $content;
    } 

    //////////////////////
    //user registration //
    /////////////////////
    public function register()
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);  
            // echo "<pre>"; print_r($clean); die;            
            $this->form_validation->set_rules('user_username', 'User Name', 'required');    
            $this->form_validation->set_rules('user_email', 'Email', 'required'); 
            $this->form_validation->set_rules('user_password', 'Password', 'required'); 
            $this->form_validation->set_rules('user_gender', 'Gender', 'required');
            if($this->form_validation->run() == FALSE) {                
                $this->session->set_flashdata('error_msg', 'The login was unsucessful');
                redirect('register');
            } 

            $checkResult = $this->AuthModel->checkUser($clean['user_email']);
            if (count($checkResult) > 0) {
                $this->session->set_flashdata('error_msg', 'This Email already exist');
                redirect('register');
            }

            $key = $this->Important_model->generate_key('user', 'user_key', 'user');
            $clean['user_key']      = $key;
            $clean['user_password'] = md5($clean['user_password']);
            $clean['user_status'] = 0;
            $clean['user_created_at'] = date('Y-m-d H:i:s');
            $result = $this->AuthModel->insert('user', $clean);
            if($result){
                $this->session->set_flashdata('success_msg', 'Register Successfully!');
                redirect('login');
            }          
        }
        else{
            $data = array();
            $data['title'] = "Registration";
            $data['meta']       = $this->template->render('segments/meta', array(), true);
            $data['headlink']   = $this->template->render('segments/headlink', array(), true);
            $data['navbar']     = $this->template->render('segments/navbar', array(), true);
            $data['footer']     = $this->template->footer(array());
            $data['footlink']   = $this->template->render('segments/footlink', array(), true);
            $data['content']    = $this->register_page_content($content_params = array());
            $view = $this->load->view('master', $data, true);
            echo $view;
            exit;
        }
    }

    public function register_page_content($content_params)
    {
        $data = array(); 
        $content =  $this->template->render('contents/auth/register',$data, true);
        return $content;
    } 


    //////////////////////
    //user site login //
    /////////////////////
    public function check_login()
    {   
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');    
        $this->form_validation->set_rules('password', 'Password', 'required'); 

        if($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', 'The login was unsucessful');
            redirect('login');
        }
        else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);    
            // echo "<pre>"; print_r($clean); die;       
            $clean['password']=md5($clean['password']);
            $userInfo = $this->AuthModel->checkLogin($clean);
            if ($userInfo) {
                    foreach ($userInfo as $key => $val) {
                    $this->session->set_userdata($key, $val);
                }

                redirect('/');
            } else {
                $this->session->set_flashdata('error_msg', 'Incorrect Email or Password!');
                redirect('login');
            }
        }
    } 

    public function logout()
    {   
        $this->session->set_userdata('user_id');
        redirect('/');
    }  

   

}